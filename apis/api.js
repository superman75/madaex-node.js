var database = require('../madaex/database');
var config   = require('../config/config');
var lib     = require('../madaex/lib');

exports.getPlayerInfo = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var playername = req.body.playername;

    if(username == undefined || password == undefined || playername == undefined) {
        var result  = {status: 'failed',  msg: 'Parameter is not valid'};
        return res.send(JSON.stringify(result));
    }

    if(username !=  'ex_to_mt_' || password != config.MADAEX_PASS) {
        var result  = {status: 'failed',  msg: 'Wrong password'};
        return res.send(JSON.stringify(result));
    }

    database.getUserInfoByUsername_api(playername, function(err, playerinfo) {
        var result  = {};
        if(err) {
            result  = {status: 'failed', msg: err.message};
            return res.send(JSON.stringify(result));
        } else if(playerinfo.length == 0) {
            result  = {status: 'failed',  msg: 'There is no user with username'};
            return res.send(JSON.stringify(result));
        }

        result = {status: 'success', msg: playerinfo[0]};
        return res.send(JSON.stringify(result));
    });
};

exports.getBalanceById = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var playerid = req.body.playerid;

    if(username == undefined || password == undefined || playerid == undefined) {
        var result  = {status: 'failed',  msg: 'Parameter is not valid'};
        return res.send(JSON.stringify(result));
    }

    if(username !=  'ex_to_mt_' || password != config.MADAEX_PASS) {
        var result  = {status: 'failed',  msg: 'Wrong password'};
        return res.send(JSON.stringify(result));
    }

    database.getUserInfoById_api(playerid, function(err, playerInfo) {
        var result  = {};
        if(err) {
            result  = {status: 'failed', msg: err.message};
            return res.send(JSON.stringify(result));
        } else if(playerInfo.length == 0) {
            result  = {status: 'failed',  msg: 'There is no user with playerid'};
            return res.send(JSON.stringify(result));
        }

        result = {status: 'success', balance: playerInfo[0].balance_satoshis};
        return res.send(JSON.stringify(result));
    });
};

exports.updateBalance = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var playerid = req.body.playerid;
    var amount = req.body.amount;

    if(username == undefined || password == undefined || playerid == undefined || amount == undefined) {
        var result  = {status: 'failed',  msg: 'Parameter is not valid'};
        return res.send(JSON.stringify(result));
    }

    if(username !=  'ex_to_mt_' || password != config.MADAEX_PASS) {
        var result  = {status: 'failed',  msg: 'Wrong password'};
        return res.send(JSON.stringify(result));
    }

    database.updateBalance_api(playerid, amount, function(err, playerInfo) {
        var result  = {};
        if(err) {
            result  = {status: 'failed', msg: err.message};
            return res.send(JSON.stringify(result));
        } else if(playerInfo.length == 0) {
            result  = {status: 'failed',  msg: 'There is no user with playerid'};
            return res.send(JSON.stringify(result));
        }

        result = {status: 'success', msg: ""};
        return res.send(JSON.stringify(result));
    });
};

exports.getCompanyBalance = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;

    if(username == undefined || password == undefined ) {
        var result  = {status: 'failed',  msg: 'Parameter is not valid'};
        return res.send(JSON.stringify(result));
    }

    if(username !=  'ex_to_mt_' || password != config.MADAEX_PASS) {
        var result  = {status: 'failed',  msg: 'Wrong password'};
        return res.send(JSON.stringify(result));
    }

    database.getCompanyBalance_api(function(err, companyInfo) {
        var result  = {};
        if(err) {
            result  = {status: 'failed', msg: err.message};
            return res.send(JSON.stringify(result));
        } else if(companyInfo.length == 0) {
            result  = {status: 'failed',  msg: 'There is no company account'};
            return res.send(JSON.stringify(result));
        }

        result = {status: 'success', msg: companyInfo[0].balance_satoshis};
        return res.send(JSON.stringify(result));
    });
};

exports.increaseCompanyBalance = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var amount = req.body.amount;

    if(username == undefined || password == undefined || amount == undefined) {
        var result  = {status: 'failed',  msg: 'Parameter is not valid'};
        return res.send(JSON.stringify(result));
    }

    if(username !=  'ex_to_mt_' || password != config.MADAEX_PASS) {
        var result  = {status: 'failed',  msg: 'Wrong password'};
        return res.send(JSON.stringify(result));
    }

    database.increaseCompanyBalance_api(amount, function(err, data) {
        var result  = {};
        if(err) {
            result  = {status: 'failed', msg: err.message};
            return res.send(JSON.stringify(result));
        } else if(data != true) {
            result  = {status: 'failed',  msg: 'There is no company account'};
            return res.send(JSON.stringify(result));
        }

        result = {status: 'success', msg: ""};
        return res.send(JSON.stringify(result));
    });
};

exports.generateAddresses = function(req, res) {
    var counts = req.counts;
};


/********************************************************/
/*********** OTCMODE.COM APIs DEFINITION ****************/
/********************************************************/
/* post a request to generate account addresses */
exports.generate_addresses = function(req, res) {
    counts = req.body.counts;
    database.generateAddresses(counts, function(error, result) {
        if (error) res.send(error);
        else res.send(result);
    });
};

/* create an account with a given id and an address */
function create_account(req, res) {
    id = req.body.id;
    address = req.body.address;
    database.createAccount(id, address, function(error, data) {
        if (error) res.send(error);
        else res.send('SUCCESS: account creation success!');
    });
}

/* delete an account with a given id and an address */
function delete_account(req, res){
    id = req.body.id;
    address = req.body.address;
    database.deleteAccount(id, address, function(error, data) {
        if (error) res.send(error);
        else res.send('SUCCESS: account delete completed!');
    });
}

/* buy tokens to a certain address */
function buy_token(req, res){
    address = req.body.address;
    amount = req.body.amount;
    database.buyToken(address, amount, function(error, data) {
        if (error) res.send(error);
        else res.send("SUCCESS: " + amount + " tokens are added to the address " + address);
    });
}

/* sell tokens from a certain address */
function sell_token(req, res){
    address = req.body.address;
    amount = req.body.amount;
    database.sellToken(address, amount, function(error, data) {
        if (error) res.send(error);
        else res.send("SUCCESS: " + amount + " tokens are decreased from the address " + address);
    });
}

/* transfer tokens from an address to another address */
function transfer_token(req, res){
    source = req.body.source;
    target = req.body.target;
    amount = req.body.amount;
    database.transferToken(source, target, amount, function(error, data) {
        if (error) res.send(error);
        else res.send('SUCCESS: Transfer ' + amount + " tokens are transferred from " + source + " to " + target);
    });
}

/* check the account if it has more balance compared to the amount */
function check_available(req, res) {
    address = req.body.address;
    amount 	= req.body.amount;
    database.checkAvailable(address, amount, function(error, data) {
        if (error) res.send(error);
        else res.send('SUCCESS: Availability - ' + data);
    });
}

// get new valid mdc account address
// created by pichmuy
exports.getNewMDCAddress = function(req, res) {
    username = req.body.username;
    password = req.body.password;
    lib.log('info', 'api.getNewMDCAddress - [begin] username:' + username +
        '   password:' + password );
    console.log('info', 'api.getNewMDCAddress - [begin] username:' + username +
        '   password:' + password );
    database.getNewMDCAddress(function(error, result) {
        if (error) {
            lib.log('info', 'api.getNewMDCAddress - [end] username:' + username +
                '   password:' + password );
            console.log('info', 'api.getNewMDCAddress - [end] username:' + username +
                '   password:' + password );
            res.json({status: 'failed', msg: error});
        }
        else {
            lib.log('info', 'api.getNewMDCAddress - [end] username:' + username +
                '   password:' + password +
                '   new address:' + result.rows);
            console.log('info', 'api.getNewMDCAddress - [end] username:' + username +
                '   password:' + password +
                '   new address:' + result.rows);
            res.send({status: 'success', msg: result});
        }
    });
};

// transfer mdc
// created by pichmuy
exports.transferToken = function(req, res) {
    var target = req.body.target;
    var source = req.body.source;
    var amount = req.body.amount;
    var username = req.body.username;
    var password = req.body.password;
    lib.log('info', 'api.transferToken - [begin] username:' + username +
        '   password:' + password +
        '   source:' + source +
        '   target:' + target +
        '   amount:' + amount );
    console.log('info', 'api.transferToken - [begin] username:' + username +
        '   password:' + password +
        '   source:' + source +
        '   target:' + target +
        '   amount:' + amount );
    database.transferToken(source, target, amount, true, function(error) {
        if (error == 'no_user') res.json({status: 'success', msg: ''});
        if (error) res.json({status: 'failed', msg: error});
        else{
            database.saveDepositHistory(source, target, amount, function(err) {
                if (err) {
                    console.log('Cannot save desposit history to database - ' + err);
                    res.json({status: 'failed', msg: err});
                }
                else {
                    console.log('Successfully saved to deposit history.');
                    res.json({status: 'success', msg: null});
                }
            });
        }
    });
};

exports.getMadabitAddress = function(req, res) {
    database.getMadabitAddress(function(error, address) {
        if (error) {
            res.send({status:'failed', msg:error});
        }
        else {
            madabit = address;
            res.send({status:'success', msg:madabit});
        }
    });
};


/*
Navicat PGSQL Data Transfer

Source Server         : Local_PostgreSQL
Source Server Version : 90606
Source Host           : localhost:5432
Source Database       : madaexdb
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90606
File Encoding         : 65001

Date: 2018-05-15 17:53:03
*/


-- ----------------------------
-- Table structure for btc_blocks
-- ----------------------------
DROP TABLE IF EXISTS "public"."btc_blocks";
CREATE TABLE "public"."btc_blocks" (
"height" int8 NOT NULL,
"hash" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of btc_blocks
-- ----------------------------

-- ----------------------------
-- Table structure for dep_withdraws
-- ----------------------------
DROP TABLE IF EXISTS "public"."dep_withdraws";
CREATE TABLE "public"."dep_withdraws" (
"id" int8 NOT NULL,
"users_id" int8,
"amount" float8,
"when_at" varchar(255) COLLATE "default",
"kind" int2,
"txid" varchar(255) COLLATE "default",
"fee" float8,
"trades_id" int8,
"direction" int2
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."dep_withdraws"."kind" IS '1:BTC, 2:ETH, 3:GP';

-- ----------------------------
-- Records of dep_withdraws
-- ----------------------------
INSERT INTO "public"."dep_withdraws" VALUES ('1', '1', '5', '2018-05-14T04:18:48.678+07:00', '3', '573ab0ce-7240-445c-ac01-e7d9f73249d7', '0.05', '1', '2');
INSERT INTO "public"."dep_withdraws" VALUES ('2', '1', '3', '2018-05-14T04:29:41.451+07:00', '3', '45196ece-b312-4dbb-b38f-038fc0a6b4be', '0.05', '1', '2');
INSERT INTO "public"."dep_withdraws" VALUES ('3', '1', '5', '2018-05-14T10:28:05.976+07:00', '3', 'a71181a1-42c6-4337-8ed0-2dfc00f08af4', '0.05', '1', '2');
INSERT INTO "public"."dep_withdraws" VALUES ('4', '2', '5', '2018-05-14T13:02:46.063+07:00', '3', '87388aa4-b3ef-48b3-a3a7-585e15cedd28', '0.025', '1', '2');

-- ----------------------------
-- Table structure for eth_blocks
-- ----------------------------
DROP TABLE IF EXISTS "public"."eth_blocks";
CREATE TABLE "public"."eth_blocks" (
"height" int8 NOT NULL,
"hash" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of eth_blocks
-- ----------------------------

-- ----------------------------
-- Table structure for gpaddresses
-- ----------------------------
DROP TABLE IF EXISTS "public"."gpaddresses";
CREATE TABLE "public"."gpaddresses" (
"id" int8 NOT NULL,
"users_id" int8,
"gp_id" int8
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."gpaddresses"."gp_id" IS 'user''s id in postgresdb--gaming site id';

-- ----------------------------
-- Records of gpaddresses
-- ----------------------------
INSERT INTO "public"."gpaddresses" VALUES ('1', '1', '5');
INSERT INTO "public"."gpaddresses" VALUES ('2', '2', '6');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS "public"."messages";
CREATE TABLE "public"."messages" (
"id" int8 NOT NULL,
"content" varchar(255) COLLATE "default",
"status" int2,
"send_at" varchar(255) COLLATE "default",
"read_at" varchar(255) COLLATE "default",
"requestcode" int2,
"postid" int8,
"sender" int8,
"receiver" int8
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."messages"."status" IS '1:unread , 2:read';
COMMENT ON COLUMN "public"."messages"."requestcode" IS '1 - 10';
COMMENT ON COLUMN "public"."messages"."postid" IS 'PostId could be tradeId depends on requestCode.';

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO "public"."messages" VALUES ('1', '{"kind":"GP","amount":"5","direction":1,"tradeId":"1","dest_addr":6}', '2', '2018-05-14T13:01:25.059+07:00', '2018-05-14T13:01:34.813+07:00', '1', '1', '2', '1');
INSERT INTO "public"."messages" VALUES ('2', '{"kind":"GP","amount":"5","tradeId":"1","direction":1,"dest_addr":1}', '2', '2018-05-14T13:01:34.822+07:00', '2018-05-14T13:01:40.562+07:00', '2', '1', '1', '2');
INSERT INTO "public"."messages" VALUES ('3', '{"tradeId":1,"amount":5,"kind":3,"seller":"test"}', '2', '2018-05-14T13:02:12.239+07:00', '', '5', '1', '1', '0');
INSERT INTO "public"."messages" VALUES ('4', '{"requestCode":6,"tradeId":1,"sellerId":2,"amount":5,"kind":3}', '2', '2018-05-14T13:02:34.610+07:00', '2018-05-14T13:02:46.055+07:00', '6', '1', '0', '2');

-- ----------------------------
-- Table structure for moneyinfo
-- ----------------------------
DROP TABLE IF EXISTS "public"."moneyinfo";
CREATE TABLE "public"."moneyinfo" (
"mid" int8 NOT NULL,
"moneyname" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of moneyinfo
-- ----------------------------
INSERT INTO "public"."moneyinfo" VALUES ('1', 'BTC');
INSERT INTO "public"."moneyinfo" VALUES ('2', 'ETH');
INSERT INTO "public"."moneyinfo" VALUES ('3', 'GP');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS "public"."posts";
CREATE TABLE "public"."posts" (
"id" int8 NOT NULL,
"users_id" int8,
"amount" float8,
"rate" float8,
"account_number" varchar(255) COLLATE "default",
"bank" varchar(255) COLLATE "default",
"dest_addr" varchar(255) COLLATE "default",
"created_at" varchar(255) COLLATE "default",
"status" int8,
"kind" int2,
"account_name" varchar(255) COLLATE "default",
"routing_number" varchar(255) COLLATE "default",
"direction" int8,
"fee" float8,
"details" text COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."posts"."status" IS '1:posted, 2:pending 3:closed';
COMMENT ON COLUMN "public"."posts"."kind" IS '1:BTC, 2:ETH, 3:GP';
COMMENT ON COLUMN "public"."posts"."direction" IS '1:buy ,2:sell';
COMMENT ON COLUMN "public"."posts"."fee" IS 'fee at that time(if buyer -1)';
COMMENT ON COLUMN "public"."posts"."details" IS '//creater''s detail text';

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO "public"."posts" VALUES ('1', '1', '10', '6.31', '', '1', '', '2018-05-14T12:57:51.933+07:00', '3', '3', '', '', '1', '0.05', 'sdfsdgdgdfg');
INSERT INTO "public"."posts" VALUES ('2', '1', '5', '6.31', '', '1', '', '2018-05-14T12:57:51.933+07:00', '1', '3', '', '', '1', '0.05', 'sdfsdgdgdfg');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS "public"."sessions";
CREATE TABLE "public"."sessions" (
"id" varchar(255) COLLATE "default" NOT NULL,
"user_id" int8,
"user_agent" varchar(255) COLLATE "default",
"ott" varchar(255) COLLATE "default",
"created_at" varchar(255) COLLATE "default",
"expire_at" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO "public"."sessions" VALUES ('0216b862-87f5-4f14-b3bb-0d5883dacaa4', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T17:02:18.690+07:00', '2018-05-15T17:02:18.690+07:00');
INSERT INTO "public"."sessions" VALUES ('02a8ae4f-c014-4f1f-a14f-66ad3d5dfb62', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T17:01:58.444+07:00', '2018-05-15T17:01:58.443+07:00');
INSERT INTO "public"."sessions" VALUES ('02d9cd44-2e88-4700-afdb-324c74d95703', '0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '2018-05-14T03:42:42.616+07:00', '2018-05-15T03:42:42.616+07:00');
INSERT INTO "public"."sessions" VALUES ('2b94a810-b59c-440c-896f-98f8af3f5081', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T17:03:28.023+07:00', '2018-05-15T17:03:28.023+07:00');
INSERT INTO "public"."sessions" VALUES ('30c505f8-e50c-4745-94cb-6ca815cadf3b', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T16:56:01.940+07:00', '2018-05-14T16:56:21.880+07:00');
INSERT INTO "public"."sessions" VALUES ('4aa79c51-ad33-4d6e-ab69-3d99cb71913b', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T03:42:29.883+07:00', '2018-05-14T12:54:25.532+07:00');
INSERT INTO "public"."sessions" VALUES ('698b817d-997d-419a-accc-5a82338786b3', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T22:53:57.719+07:00', '2018-05-15T22:53:57.718+07:00');
INSERT INTO "public"."sessions" VALUES ('6dd245b5-9276-4f82-9f16-e7a7a7bd115b', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:13:59.497+07:00', '2018-05-15T17:13:59.497+07:00');
INSERT INTO "public"."sessions" VALUES ('71273324-7335-4c6a-8cf7-dfe1f1f0733e', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:09:01.006+07:00', '2018-05-15T17:09:01.006+07:00');
INSERT INTO "public"."sessions" VALUES ('84babdad-c1c7-48ab-8a18-a480be23cf4a', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T12:55:38.614+07:00', '2018-05-14T12:55:40.649+07:00');
INSERT INTO "public"."sessions" VALUES ('86681c02-f0e7-48d6-8a58-07ad7306876f', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T12:56:40.398+07:00', '2018-05-15T12:56:40.398+07:00');
INSERT INTO "public"."sessions" VALUES ('a0da9fb5-37a4-461f-a2d5-4fcaa2f923a6', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:09:52.676+07:00', '2018-05-15T17:09:52.676+07:00');
INSERT INTO "public"."sessions" VALUES ('ae5291f9-520c-4671-a1ee-ac5abb4eb573', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T17:11:17.740+07:00', '2018-05-15T17:11:17.740+07:00');
INSERT INTO "public"."sessions" VALUES ('af62abcd-0cd4-4acf-8cc7-3d8b2744e8f1', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T16:59:38.488+07:00', '2018-05-15T16:59:38.488+07:00');
INSERT INTO "public"."sessions" VALUES ('c3554ac7-158b-4e4c-a1ee-fb985eb1d745', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:18:44.414+07:00', '2018-05-15T17:18:44.414+07:00');
INSERT INTO "public"."sessions" VALUES ('cff94305-4308-4e8f-9f0f-2cf208380189', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T03:43:56.481+07:00', '2018-05-14T12:54:29.311+07:00');
INSERT INTO "public"."sessions" VALUES ('d3501d1b-98da-4f82-8a95-9e6399caab80', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:15:24.007+07:00', '2018-05-15T17:15:24.007+07:00');
INSERT INTO "public"."sessions" VALUES ('e21e96e0-7eba-458f-8999-8246d578d8f9', '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', '2018-05-14T12:56:15.719+07:00', '2018-05-14T16:56:21.880+07:00');
INSERT INTO "public"."sessions" VALUES ('eee47025-0a32-4ccd-a00a-c73d1fca2ba3', '2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36', '', '2018-05-14T17:12:37.719+07:00', '2018-05-15T17:12:37.719+07:00');

-- ----------------------------
-- Table structure for supports
-- ----------------------------
DROP TABLE IF EXISTS "public"."supports";
CREATE TABLE "public"."supports" (
"id" int8 NOT NULL,
"title" varchar(255) COLLATE "default",
"content" text COLLATE "default",
"received_at" varchar(255) COLLATE "default",
"users_id" int8,
"status" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of supports
-- ----------------------------

-- ----------------------------
-- Table structure for trades
-- ----------------------------
DROP TABLE IF EXISTS "public"."trades";
CREATE TABLE "public"."trades" (
"id" int8 NOT NULL,
"post_id" int8,
"seller" int8,
"buyer" int8,
"amount" float8,
"created_at" varchar(255) COLLATE "default",
"status" int8,
"kind" int2,
"dest_addr" varchar(255) COLLATE "default",
"fee" float8
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."trades"."status" IS '1:trade 2:cancel 3:block 4:complete 5:sent verify';
COMMENT ON COLUMN "public"."trades"."kind" IS '1:BTC, 2:ETH, 3:GP';
COMMENT ON COLUMN "public"."trades"."fee" IS 'fee at that time';

-- ----------------------------
-- Records of trades
-- ----------------------------
INSERT INTO "public"."trades" VALUES ('1', '1', '2', '1', '5', '2018-05-14T13:01:34.824+07:00', '4', '3', '5', '0.025');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
"id" int8 DEFAULT 1 NOT NULL,
"email" varchar(255) COLLATE "default",
"phone_number" varchar(255) COLLATE "default",
"user_name" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"created_at" varchar(255) COLLATE "default",
"balance_btc" float8 DEFAULT 0,
"balance_eth" float8 DEFAULT 0,
"balance_gp" float8 DEFAULT 0,
"dialcode" char(255) COLLATE "default",
"verified" int2,
"verifycode" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."users"."verified" IS '0:pending(waiting for verify code) 1:normal user';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('0', 'p.chakra115@outlook.com', '(+855)69845910', 'madaexadmin', 'sha1$6a55682f$1$b9e0bcb98723376f22f499c9fdc808802d377e74', '2018-05-13T10:49:29.287+07:00', '0', '0', '0', '855                                                                                                                                                                                                                                                            ', '1', '9345');
INSERT INTO "public"."users" VALUES ('1', 'jungle.rush@outlook.com', '(+855)69845910', 'user_seller', 'sha1$116fc839$1$56c43d528b151276ba8bed0c833fe4c1ec885fd2', '2018-05-13T10:53:41.842+07:00', '0', '0', '0', '855                                                                                                                                                                                                                                                            ', '1', '3643');
INSERT INTO "public"."users" VALUES ('2', 'jr19861206@gmail.com', '(+855)98487544', 'test', 'sha1$5971b7df$1$09b7e795bf7ad1e13bf21f476d3a4d7741d40a08', '2018-05-13T10:56:36.001+07:00', '0', '0', '0', '855                                                                                                                                                                                                                                                            ', '1', '3545');

-- ----------------------------
-- Table structure for verifications
-- ----------------------------
DROP TABLE IF EXISTS "public"."verifications";
CREATE TABLE "public"."verifications" (
"id" int8 NOT NULL,
"url" varchar(255) COLLATE "default",
"trades_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of verifications
-- ----------------------------
INSERT INTO "public"."verifications" VALUES ('1', '/upload/9a1f1d0d-2aa0-4997-a989-aa257dbd3369eth.txt', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table btc_blocks
-- ----------------------------
ALTER TABLE "public"."btc_blocks" ADD PRIMARY KEY ("height");

-- ----------------------------
-- Primary Key structure for table dep_withdraws
-- ----------------------------
ALTER TABLE "public"."dep_withdraws" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table eth_blocks
-- ----------------------------
ALTER TABLE "public"."eth_blocks" ADD PRIMARY KEY ("height");

-- ----------------------------
-- Primary Key structure for table gpaddresses
-- ----------------------------
ALTER TABLE "public"."gpaddresses" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table messages
-- ----------------------------
ALTER TABLE "public"."messages" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table moneyinfo
-- ----------------------------
ALTER TABLE "public"."moneyinfo" ADD PRIMARY KEY ("mid");

-- ----------------------------
-- Primary Key structure for table posts
-- ----------------------------
ALTER TABLE "public"."posts" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sessions
-- ----------------------------
ALTER TABLE "public"."sessions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table supports
-- ----------------------------
ALTER TABLE "public"."supports" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trades
-- ----------------------------
ALTER TABLE "public"."trades" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table verifications
-- ----------------------------
ALTER TABLE "public"."verifications" ADD PRIMARY KEY ("id");

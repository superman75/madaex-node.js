var forexDB = require('./forexdb');
var database = require('./database');
var forexLib = require('./forexlib');

exports.pay = function (userName, payInfo, callback) {
    forexDB.checkBalanceMDC(userName, payInfo.amount, function (err) {
        if (err) {
            console.log('forex_mdc.pay - error - checkBalanceMDC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            forexLib.log('error', 'forex_mdc.pay - checkBalanceMDC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            return callback(err);
        }

        forexDB.payMDC(userName, payInfo.amount, function (err) {
            if (err) {
                console.log('forex_mdc.pay - error - payMDC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                forexLib.log('error', 'forex_mdc.pay - payMDC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                return callback(err);
            }

            forexDB.getUserMDCAddress(userName, function (err, source) {
                if (err) {
                    console.log('forex_mdc.pay - error - getUserMDCAddress - userName:' + userName + '   err:' + err);
                    forexLib.log('error', 'forex_mdc.pay - getUserMDCAddress - userName:' + userName + '   err:' + err);
                    return callback(err);
                }

                database.transferToken(source, payInfo.address, payInfo.amount, false, function (err) {
                    if (err) {
                        console.log('forex_mdc.pay - error - database.transferToken - from:' + source + '   amount:' + payInfo.amount + '   to:' + payInfo.address + '   err:' + err);
                        forexLib.log('error', 'forex_mdc.pay - database.transferToken - from:' + source + '   amount:' + payInfo.amount + '   to:' + payInfo.address + '   err:' + err);
                        return callback(err);
                    }

                    console.log('forex_mdc.pay - success - database.transferToken - from:' + source + '   amount:' + payInfo.amount + '   to:' + payInfo.address);
                    forexLib.log('success', 'forex_mdc.pay - database.transferToken - from:' + source + '   amount:' + payInfo.amount + '   to:' + payInfo.address);

                    return callback(null);
                });
            });
        });
    });
};

exports.checkUpdateMDC = function (target, amount, callback) {
    forexDB.getPayInfo(target, function (err, result) {
        if (err) {
            console.log('forex_mdc.checkUpdateMDC - error - getPayInfo - err:' + err);
            forexLib.log('error', 'forex_mdc.checkUpdateMDC - getPayInfo - err:' + err);
            return callback(err);
        }

        var payInfo = {};
        payInfo.userId = result.user_id;
        payInfo.coinType = result.coin_type;
        payInfo.crm_order_number = result.crm_order_number;
        payInfo.amount = amount;

        forexDB.updateFunding(payInfo, function (err, postInfo) {
            if (err) {
                console.log('forex_mdc.checkUpdateMDC - error - updateFunding - payInfo - userId:' + payInfo.userId + '   coinType:' + payInfo.coinType + '   amount:' + payInfo.amount + '   err:' + err);
                forexLib.log('error', 'forex_mdc.checkUpdateMDC - updateFunding - payInfo - userId:' + payInfo.userId + '   coinType:' + payInfo.coinType + '   amount:' + payInfo.amount + '   err:' + err);
                return callback(err);
            }

            console.log('forex_mdc.checkUpdateMDC - success - updateFunding - payInfo - userId:' + payInfo.userId + '   coinType:' + payInfo.coinType + '   amount:' + payInfo.amount);
            forexLib.log('success', 'forex_mdc.checkUpdateMDC - success - updateFunding - payInfo - userId:' + payInfo.userId + '   coinType:' + payInfo.coinType + '   amount:' + payInfo.amount);

            forexDB.checkSelfPost(postInfo.funding_id, postInfo.postParam, function (err) {
                if (err) return callback(err);
                return callback(null);
            });
        });
    });
};

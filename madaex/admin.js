let database    = require('./database');
// set user's name verification
exports.nameVerify = function(req , res , next){
    let user_id = req.body.user_id;
    let verified = req.body.verified;
    console.log('you are verifying this user ' + user_id + ' ' + verified);
    database.setNameVerify(user_id, verified, function(result){
        res.json(result);
    })
};
// set exchange fee of this site
exports.setFee = function(req , res , next)
{
    let user = req.user;
    if(isAdmin(req))
    {
        let market = req.body.market;
        let fee = req.body.fee;
        let feeKey = market.toLowerCase() + '_fee';
        let param = {
            key : feeKey,
            value : fee
        };
        console.log(param);
        database.setCommon(param, function(result){
            res.json(result);
        })
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};

// set exchange rate of this site
exports.setExCost = function(req , res , next)
{
    let user = req.user;
    if(isAdmin(req))
    {
        let market = req.body.market;
        let cost = req.body.cost;
        let costKey = 'ex_' + market.toLowerCase() + '_cost';
        let param = {
            key : costKey,
            value : cost
        };
        database.setCommon(param, function(result){
            res.json(result);
        })
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};


// get company wallet information
exports.getCompanyWalletInfo = function(req, res, next)
{
    let user = req.user;
    if(isAdmin(req))
    {
        let companyInfo = {};
        companyInfo.btc = user.balance_btc;
        companyInfo.eth = user.balance_eth;
        companyInfo.mdc = user.balance_mdc;
        res.json({status : 'success' , msg : companyInfo});
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};

// get deposit data from database
exports.getDepositeInfo = function(req ,res ,next)
{
    // console.log('---------------------getdepositeinfo---------------------');
    var user = req.user;
    if(isAdmin(req))
    {
        var searchName = req.body.searchName;
        var searchKind = req.body.searchKind;
        database.getDepositeInfo(searchName , searchKind , function(result){
            res.json(result);
        });
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};
// get deposit and withdrawal statistic data
exports.getStatistic = function(req, res)
{
    if(isAdmin(req))
    {
        let from = req.body.from;
        let to = req.body.to;
        console.log('--------------------------------getStatistic-----------------------');
        console.log(from);
        console.log(to);
        database.getStatistic(from , to , function(res1){
            res.json(res1);
        });
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};

// get transaction total amount
exports.getTotalAmountInfo = function(req, res, next)
{
    var user = req.user;
    if(isAdmin(req))
    {
        database.getTotalAmount(function(result){
            if(result.status === 'success')
            {
                //this result could not include all kind data info
                //you should keep in mind this problem
                //i solved like this
                // console.log('-----------------------gettotal-----------------------------');
                let totalInfo = {
                    btc : 0,
                    eth : 0,
                    mdc : 0
                };
                let total = result.msg;
                console.log(total);
                for(let i = 0 ; i < total.length ; i ++)
                {
                    if(total[i].market === "BTC")
                        totalInfo.btc = total[i].sum;
                    else if (total[i].market === "ETH")
                        totalInfo.eth = total[i].sum;
                    else  totalInfo.mdc = total[i].sum;
                }
                console.log(totalInfo);

                res.json({status : 'success' , msg : totalInfo});

            }
            else
            {
                res.json(result);
            }
        });
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin'});
    }
};

// withdraw

exports.companyWithdraw = function(req , res , next)
{
    // console.log(req);
    if(isAdmin(req))
    {
        var userid = req.user.id;
        var amount = req.body.amount;
        var address = req.body.address;
        var kind = req.body.kind;

        var estimation = amount + config.MINING_FEE;
        var available = req.user.balance_btc;
        if(estimation < available)
        {
            withdraw.withdraw(userid , '' , amount , address , kind , -1 , 0 , function(result)
            {
                // console.log(result);
                var amountToDec = estimation;

                if(param.kind === 1)
                {
                    database.decreaseBalanceByBTC({balance_btc:amountToDec, id:userid} , function(result1){
                        if(result1 == "success")
                            res.json({status : "success" , msg : ""});
                        else
                            res.json({status : "failed" , msg : "Error occured when decrease funds."});
                    });
                }
                if(param.kind === 2)
                {
                    database.decreaseBalanceByETH({balance_eth:amountToDec, id:userid} , function(result1){
                        if(result1 == "success")
                            res.json({status : "success" , msg : ""});
                        else
                            res.json({status : "failed" , msg : "Error occured when decrease funds."});
                    });
                }
            });
        }
        else
        {
            res.json({status : "failed" , msg : "Available amount is smaller than amount to send."});
        }
    }
    else
    {
        res.json({status : 'failed' , msg : 'You are not admin. Please login again.'});
    }
};

// check if this user is admin.
function isAdmin(req)
{
    if(!isLogged(req))
        return false;
    else return req.user.id === 1;
}
// check if this user is logged.
function isLogged(req)
{
    return req.user !== undefined;
}
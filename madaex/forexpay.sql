CREATE TABLE forexfundings
(
    id bigserial NOT NULL PRIMARY KEY,
    user_id int8,
    coin_type text,
    address text,
    crm_order_number text,
    notify_url text,
    amount_req float8,
    sign_req text,
    time_req timestamp with time zone DEFAULT now() NOT NULL,
    amount_pay float8 DEFAULT 0,
    sign_pay text,
    time_pay timestamp with time zone,
    status int8 DEFAULT 0,
    last_check timestamp with time zone DEFAULT now() NOT NULL
);

CREATE TABLE forexaddrbtc
(
    id bigserial NOT NULL PRIMARY KEY,
    address text
);

CREATE TABLE forexaddrbtc_test
(
    id bigserial NOT NULL PRIMARY KEY,
    address text
);

CREATE TABLE forexaddrmdc
(
    id bigserial NOT NULL PRIMARY KEY,
    address text
);

CREATE TABLE forexaddrmdc_test
(
    id bigserial NOT NULL PRIMARY KEY,
    address text
);

CREATE TABLE forex_btc_blocks (
    height int8 NOT NULL,
    hash text NOT NULL
);

ALTER TABLE ONLY forex_btc_blocks
    ADD CONSTRAINT bv_forex_btc_blocks_pkey PRIMARY KEY (height, hash);


CREATE TABLE forex_eth_blocks (
    height int8 NOT NULL,
    hash text NOT NULL
);

ALTER TABLE ONLY forex_eth_blocks
    ADD CONSTRAINT bv_forex_eth_blocks_pkey PRIMARY KEY (height, hash);

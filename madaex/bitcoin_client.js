var bitcoin = require('bitcoin');

var client = new bitcoin.Client({
    host: process.env.BITCOIND_HOST || 'localhost',
    port: process.env.BITCOIND_PORT || 8332,
    user: process.env.BITCOIND_USER || 'username',
    pass: process.env.BITCOIND_PASS || 'password',
    timeout: 240000
});

// console.log(client);

module.exports = client;
var database = require('./database');
var lib      = require('./lib');
var fs       = require('fs');

console.log('========== mdc address import started ==========');
lib.log('info', '========== mdc address import started ==========');

console.log('info : ', 'read addresses from json file');
lib.log('info', 'read addresses from json file');
var depositAddresses = {};
depositAddresses['MDC'] = {};
if (process.env.TESTNET == 1) {
    depositAddresses['MDC'] = JSON.parse(fs.readFileSync('1addresses_mdc_testnet.json', 'utf8'));
}
else {
    depositAddresses['MDC'] = JSON.parse(fs.readFileSync('1addresses_mdc.json', 'utf8'));
}

var swapedDepositAddresses    = {};
swapedDepositAddresses['MDC'] = {};

for(var depAddress in depositAddresses['MDC']) {
    swapedDepositAddresses['MDC'][depositAddresses['MDC'][depAddress]] = depAddress;
}

database.addressInitialization(swapedDepositAddresses['MDC'], function(err) {

    if (err) {
        console.log('error : ', 'mdc addresses import failed -> ' + err);
        lib.log('error', 'mdc addresses import failed -> ' + err, function(err){
            process.exit();
        });
    }
    else {
        console.log('success : ', 'mdc addresses are successfully imported');
        lib.log('success', 'mdc addresses are successfully imported', function(err){
            process.exit();
        });
    }
});

// var bc = require('./bitcoin_client');
var forexDB = require('./forexdb');
var forexLib = require('./forexlib');

exports.pay = function (userName, payInfo, callback) {
    forexDB.checkBalanceBTC(userName, payInfo.amount, function (err) {
        if (err) {
            console.log('forex_btc.pay - error - checkBalanceBTC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            forexLib.log('error', 'forex_btc.pay - checkBalanceBTC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            return callback(err);
        }

        // bc.sendToAddress(payInfo.address, payInfo.amount, function (err, hash) {
        //     if (err) return callback(err);
        //     forexDB.payBTC(userName, amount, function (err) {
        //         if (err) return callback(err);
        //         return callback(null, hash);
        //     });
        // });

        forexDB.payBTC(userName, payInfo.amount, function (err) {
            if (err) {
                console.log('forex_btc.pay - error - payBTC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                forexLib.log('error', 'forex_btc.pay - payBTC - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                return callback(err);
            }
            // return callback(null/*, hash*/);

            forexDB.updateFunding(payInfo, function (err/*, payResult */) {
                if (err) {
                    console.log('forex_btc.pay - error - updateFunding - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                    forexLib.log('error', 'forex_btc.pay - updateFunding - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                    return callback(err);
                }

                console.log('forex_btc.pay - updateFunding - success - userName:' + userName + '   amount:' + payInfo.amount);
                forexLib.log('success', 'forex_btc.pay - updateFunding - userName:' + userName + '   amount:' + payInfo.amount);

                return callback(null/*, payResult */);
            });
        });
    });
};

var assert = require('assert');
var nodemailer = require('nodemailer');
var sesTransport = require('nodemailer-ses-transport');
var config = require('../config/config');

var SITE_URL = config.SITE_URL;


// function send(details, callback) {
//     assert(details, callback);
//
//     var transport = nodemailer.createTransport(sesTransport({
//         AWSAccessKeyID: config.AWS_SES_KEY,
//         AWSSecretKey: config.AWS_SES_SECRET
//     }));
//
//     transport.sendMail(details, function(err) {
//         if (err)
//             return callback(err);
//
//         callback(null);
//     });
// }

function send(details, callback) {

    // nodemailer.createTestAccount((err, account) => {
        // console.log("\n  Now in the nodemailer.createTestAccount function.");
        // console.log("\n  Error\n" + err);
        // console.log("\n  Account information");
        //console.log(account);
        // if (err) {
        //     console.error('\n  Failed to create a testing account. ' + err.message);
        //     return process.exit(1);
        // }

        //console.log('\n  Credentials obtained, sending message...');

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport({
            // host:  'smtp.gmail.com',
            // port: 465,
            // secure: false,
            service: 'Gmail',//use ssl
            auth: {
                user: 'softbank.chour@gmail.com',
                pass: 'prumchakra19'
            }
        });

        // Message object
        let message = {
            from: 'OTCMODE Support',
            to: '<' + details.to + '>',
            subject: 'Welcome to our site',
            // text: 'Hello to Pich Muy!\n This is Nils Jansson.\nLong time no see.',
            html: details.html
        };

        transporter.sendMail(message, (err, info) => {
            if (err) {
                console.log('\n  Error occurred 1. ' + err.message);
                // return process.exit(1);
                return callback(err , info)
            }

            // console.log('\n  Message Info');
            // console.log(info);
            // console.log("\n\n");
            // console.log('Message sent: %s', info.messageId);
            // // Preview only available when sending through an Ethereal account
            // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            callback(null , info);
        });
    // });
}
/**********************This is for mail reply begin**************************************/
exports.reply = function(to , content ,callback) {

    var details = {
        to: to,
        from: 'softbank.chour@gamil.com',
        html: '<!DOCTYPE>' +
                '<html>' +
                    '<head>' +
                    '</head>' +
                    '<body>' +
                        '<h2>Thank you for contacting us</h2>' +
                        '<h3>Reply for your question</h3>' +
                        '<b>' + content + '</b>' +
                    '</body>' +
                '</html>'
    };
    send(details, function(err, result) {
        callback(err, result);
    });
};
/***************************This is for mail reply end********************************/
/**********************This is for mail reply begin**************************************/
exports.sendNewPassword = function(to , content ,callback) {

    var details = {
        to: to,
        from: 'softbank.chour@gamil.com',
        html: '<!DOCTYPE>' +
                '<html>' +
                    '<head>' +
                    '</head>' +
                    '<body>' +
                        '<h2>Be carefull when use your password.</h2>' +
                        '<h3>Your new password is</h3>' +
                        '<b>' + content + '</b>' + '<br>' +
                        '<sm>Please change this password as soon as possible.</sm>' +
                    '</body>' +
                '</html>'
    };
    send(details, function(err, result) {
        callback(err, result);
    });
};
/***************************This is for mail reply end********************************/
/**************************This is for verify code begin*****************************/
exports.contact = function(to, content,callback) {

    let details = {
        to: to,
        from: 'softbank.chour@gamil.com',
        html: '<!DOCTYPE>' +
                '<html>' +
                    '<head>' +
                    '</head>' +
                    '<body>' +
                        '<h2>Email verification for OTCMODE</h2>' +
                        '<a href="http://otcmode.com/wallet?verifycode='+content+'">click here</a>' +
                    '</body>' +
                '</html>'
    };
    send(details, function(err, result) {
        callback(err, result);
    });
};

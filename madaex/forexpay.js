// *** Add following functions to routes.js
// app.post('/forexpay', forexpay.forexpay);
// app.get('/forexpay', forexpay.forexpay_lang);
// app.post('/requestVerifyCode', forexpay.requestVerifyCode);
// app.post('/confirmVerifyCode', forexpay.confirmVerifyCode);
// app.post('/checkExternalPayment', forexpay.checkExternalPayment);
// app.post('/saveParams', forexpay.saveParams);

var forexLib = require('./forexlib');
var forexBTC = require('./forexbtc');
var forexETH = require('./forexeth');
var forexMDC = require('./forexmdc');
var forexCfg = require('./forexcfg');
var forexDB = require('./forexdb');
var qr = require('qr-image');
var request = require('request');
var querystring = require('querystring');
var md5 = require('md5');

var otcURL;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_LOCAL) otcURL = forexCfg.OTC_URL_LOCAL;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_TEST) otcURL = forexCfg.OTC_URL_TEST;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_WINDOWS) otcURL = forexCfg.OTC_URL_WINDOWS;

var isETHAddress = function (address) {
    if (address.length !== 42) return false;
    if (address.substr(0, 2) != '0x') return false;

    address = address.toLowerCase();
    if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
        return false;
    }

    return true;
};

var forexpay = function (req, res, next) {
    var coinType = forexLib.removeNullsAndTrim(req.body.coin_type);
    var amount = forexLib.removeNullsAndTrim(req.body.amount);
    var userId = forexLib.removeNullsAndTrim(req.body.user_id);
    var userETHAddress = forexLib.removeNullsAndTrim(req.body.user_eth_address);
    var crmOrderNumber = forexLib.removeNullsAndTrim(req.body.crm_order_number);
    var notifyURL = forexLib.removeNullsAndTrim(req.body.notify_url);
    var sign = forexLib.removeNullsAndTrim(req.body.sign);

    if (coinType === null || coinType === undefined || coinType === '' ||
        amount === null || amount === undefined || amount === '' ||
        userId === null || userId === undefined || userId === '' ||
        crmOrderNumber === null || crmOrderNumber === undefined || crmOrderNumber === '' ||
        notifyURL === null || notifyURL === undefined || notifyURL === '' ||
        sign === null || sign === undefined || sign === '') {
        console.log('forex - forexpay - error - PARAM NOT ENOUGH - coinType:' + coinType + '   amount:' + amount + '   userId:' + userId + 'crmOrderNumber:' + crmOrderNumber + '   notifyURL:' + notifyURL + '   sign:' + sign);
        forexLib.log('error', 'forex - forexpay - PARAM NOT ENOUGH - coinType:' + coinType + '   amount:' + amount + '   userId:' + userId + 'crmOrderNumber:' + crmOrderNumber + '   notifyURL:' + notifyURL + '   sign:' + sign);

        return res.render('forexerr', { status: 'PARAM NOT ENOUGH' });
    }

    coinType = coinType.toUpperCase();
    if (coinType !== 'BTC' && coinType !== 'ETH' && coinType !== 'MDC') {
        console.log('forex - forexpay - error - COIN_TYPE_NOT_VALID - coinType:' + coinType);
        forexLib.log('error', 'forex - forexpay - COIN_TYPE_NOT_VALID - coinType:' + coinType);

        return res.render('forexerr', { status: 'COIN_TYPE_NOT_VALID' });
    }

    if (coinType === 'ETH' && (userETHAddress === null || userETHAddress === undefined || userETHAddress === '')) {
        console.log('forex - forexpay - error - USER_ETH_ADDRESS_NOT_SET');
        forexLib.log('error', 'forex - forexpay - USER_ETH_ADDRESS_NOT_SET');
        return res.render('forexerr', { status: 'USER_ETH_ADDRESS_NOT_SET' });
    }

    if (coinType === 'ETH' && isETHAddress(userETHAddress) === false) {
        console.log('forex - forexpay - error - USER_ETH_ADDRESS_INVALID');
        forexLib.log('error', 'forex - forexpay - USER_ETH_ADDRESS_INVALID');
        return res.render('forexerr', { status: 'USER_ETH_ADDRESS_INVALID' });
    }

    amount = parseFloat(amount);
    if (isNaN(amount) || amount <= 0) {
        console.log('forex - forexpay - error - AMOUNT_NOT_VALID - amount:' + amount);
        forexLib.log('error', 'forex - forexpay - AMOUNT_NOT_VALID - amount:' + amount);

        return res.render('forexerr', { status: 'AMOUNT_NOT_VALID' });
    }

    userId = parseInt(userId);
    if (isNaN(userId) || userId < 1 || userId > 1000000) {
        console.log('forex - forexpay - error - USER_ID_NOT_VALID - userId:' + userId);
        forexLib.log('error', 'forex - forexpay - USER_ID_NOT_VALID - userId:' + userId);

        return res.render('forexerr', { status: 'USER_ID_NOT_VALID' });
    }

    // check sign
    sign = sign.toLowerCase();
    var seed = crmOrderNumber + forexCfg.SIGN_KEY;
    var localSign = md5(seed);
    localSign = localSign.toLowerCase();

    if (sign !== localSign) {
        console.log('forex - forexpay - error - SIGN_NOT_MATCH - sign:' + sign);
        forexLib.log('error', 'forex - forexpay - SIGN_NOT_MATCH - sign:' + sign);

        return res.render('forexerr', { status: 'SIGN_NOT_MATCH' });
    }

    var payInfo = {};
    payInfo.coinType = coinType;
    payInfo.amount = amount;
    payInfo.userId = userId;
    payInfo.crmOrderNumber = crmOrderNumber;
    payInfo.notifyURL = notifyURL;
    payInfo.sign = sign;

    if (coinType === 'BTC') {
        forexDB.getBTCDepositAddress(userId, function (err, btcAddress) {
            if (err) {
                console.log('forex - forexpay - error - get btc address - error:' + err);
                forexLib.log('error', 'forex - forexpay - get btc address - error:' + err);

                return res.render('forexerr', { status: 'BTC_ADDRESS_ERROR' });
            }

            payInfo.address = btcAddress;
            return doPay(res, payInfo);
        });
    } else if (coinType === 'ETH') {
        payInfo.address = userETHAddress;
        return doPay(res, payInfo);
    } else if (coinType === 'MDC') {
        forexDB.getMDCDepositAddress(userId, function (err, mdcAddress) {
            if (err) {
                console.log('forex - forexpay - error - get mdc address - error:' + err);
                forexLib.log('error', 'forex - forexpay - get mdc address - error:' + err);

                return res.render('forexerr', { status: 'MDC_ADDRESS_ERROR' });
            }

            payInfo.address = mdcAddress;
            return doPay(res, payInfo);
        });
    }
};
exports.forexpay = forexpay;

exports.forexpay_lang = function (req, res, next) {
    forexDB.loadParams(function (err, params) {
        if (err)
        {
            console.log('forex - forexpay_lang - error - loadParams - error:', err);
            forexLib.log('error', 'forex - forex_lang - loadParams - error:', err);

            return callback(err);
        }

        req.body.coin_type = params.coinType;
        req.body.amount = params.amount;
        req.body.user_id = params.userId;
        req.body.user_eth_address = params.userETHAddress;
        req.body.crm_order_number = params.crmOrderNumber;
        req.body.notify_url = params.notifyURL;
        req.body.sign = params.sign;

        forexpay(req, res, next);
    });
}

var doPay = function (res, payInfo) {
    forexDB.createFunding(payInfo, function (err) {
        if (err) {
            console.log('forex - do_pay - error - create_funding - error:', err);
            forexLib.log('error', 'forex - do_pay - create_funding - error:', err);

            return res.render('forexerr', { status: 'CREATE_FUNDING' });
        }

        console.log('forex - do_pay - success - create_funding - pay-info - coinType:' + payInfo.coinType + '   amount:' + payInfo.amount + '   userId:' + payInfo.userId + '   crmOrderNumber:' + payInfo.crmOrderNumber + '   notifyURL:' + payInfo.notifyURL + '   sign:' + payInfo.sign + '   address:' + payInfo.address);
        forexLib.log('success', 'forex - do_pay - create_funding - pay-info - coinType:' + payInfo.coinType + '   amount:' + payInfo.amount + '   userId:' + payInfo.userId + '   crmOrderNumber:' + payInfo.crmOrderNumber + '   notifyURL:' + payInfo.notifyURL + '   sign:' + payInfo.sign + '   address:' + payInfo.address);

        var svgAddress;
        if (payInfo.coinType === 'ETH') {
            svgAddress = qr.imageSync(forexCfg.ETH_COMPANY_ADDRESS, { type: 'svg', size: 5 });
        } else {
            svgAddress = qr.imageSync(payInfo.address, { type: 'svg', size: 5 });
        }

        return res.render('forexpay', {
            payInfo: payInfo,
            svgqr: svgAddress,
            otcURL: otcURL,
            ethCompanyAddress: forexCfg.ETH_COMPANY_ADDRESS
        });
    });
};

exports.requestVerifyCode = function (req, res) {
    var userName = forexLib.removeNullsAndTrim(req.body.userName);
    var password = forexLib.removeNullsAndTrim(req.body.password);
    var strVerifyCode = forexLib.getPhoneVerifyCode();

    forexDB.requestVerifyCode(userName, password, strVerifyCode, function (err, phoneNumber) {
        if (err) {
            console.log('forex - request_verify_code - error - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   error:' + err);
            forexLib.log('error', 'forex - request_verify_code - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   error:' + err);

            if (err === 'USER_NOT_EXISTS' || err === 'WRONG_PASSWORD')
            {
                return res.send('LOGIN_FAILED');
            }

            return res.send('FAILED');
        }

        console.log('forex - request_verify_code - success - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode);
        forexLib.log('success', 'forex - request_verify_code - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode);

        sendVerificationCode(phoneNumber, strVerifyCode, req.i18n_lang, function (err, sendResult) {
            if (err || parseInt(sendResult) < 0) {
                console.log('forex - request_verify_code - send_verification_code - error - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   phone_number:' + phoneNumber + '   error:' + err);
                forexLib.log('error', 'forex - request_verify_code - send_verification_code - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   phone_number:' + phoneNumber + '   error:' + err);

                return res.send('FAILED');
            }

            console.log('forex - request_verify_code - send_verification_code - success - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   phone_number:' + phoneNumber);
            forexLib.log('success', 'forex - request_verify_code - send_verification_code - user_name:' + userName + '   password:' + password + '   verify_code:' + strVerifyCode + '   phone_number:' + phoneNumber);

            return res.send('SUCCESS');
        });
    });
};

function sendVerificationCode (strPhoneNumber, strVerificationCode, strCodec, callback) {
    var codec;
    var strMsg;
    if (strCodec === 'en') {
        codec = '0';
        strMsg = 'OTC Verification Code is ' + strVerificationCode;
        strMsg = Buffer.from(strMsg, 'utf8').toString('hex');
    } else if (strCodec === 'zh') {
        codec = '8';
        var strVHCode = Buffer.from(strVerificationCode, 'utf8').toString('hex');
        var nLen = strVHCode.length;

        var strUTF16BE = '';
        for (var nId = 0; nId < nLen; nId += 2) {
            strUTF16BE += '00' + strVHCode.substr(nId, 2);
        }
        strMsg = '004D0041004400410042004900549A8C8BC17801FF1A0020' + strUTF16BE + '002030029A8C8BC178016709654800355206949F0020301075AF70B93011';
    }

    var form = {
        Src: 'beneforex2018',
        Pwd: 'baofu123',
        Dest: strPhoneNumber,
        Codec: codec,
        Msg: strMsg,
        Servicesid: 'SEND'
    };

    var formData = querystring.stringify(form);
    var contentLength = formData.length;

    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: 'http://m.isms360.com:8085/mt/MT3.ashx',
        body: formData,
        method: 'POST'
    }, function (err, res, body) {
        if (err) {
            console.log('forex - sms - send verification code - send verification code - error:' + err);
            forexLib.log('error', 'forex - sms - send verification code - error:' + err);
            return callback(err);
        }

        var smsResult = parseInt(body);
        if (smsResult < 0) {
            console.log('forex - sms - send verification code - failed - phone_number:' + strPhoneNumber + '   verify_code:' + strVerificationCode + '   reason:' + body);
            forexLib.log('failed', 'forex - sms - send verification code - send verification code - phone_number:' + strPhoneNumber + '   verification_code:' + strVerificationCode + '   reason:' + body);
        } else {
            console.log('forex - sms - success - phone_number:' + strPhoneNumber + '   verify_code:' + strVerificationCode + '   service_msg_id:' + body);
            forexLib.log('success', 'forex - sms - send verification code - phone_number:' + strPhoneNumber + '   verification_code:' + strVerificationCode + '   service_msg_id:' + body);
        }

        return callback(null, body);
    });
}

exports.confirmVerifyCode = function (req, res) {
    var userName = forexLib.removeNullsAndTrim(req.body.userName);
    var password = forexLib.removeNullsAndTrim(req.body.password);
    var verifyCode = forexLib.removeNullsAndTrim(req.body.verifyCode);

    var payInfo = {};
    payInfo.coinType = forexLib.removeNullsAndTrim(req.body.coin_type);
    payInfo.amount = forexLib.removeNullsAndTrim(req.body.amount);
    payInfo.userId = forexLib.removeNullsAndTrim(req.body.user_id);
    payInfo.address = forexLib.removeNullsAndTrim(req.body.address);
    payInfo.crmOrderNumber = forexLib.removeNullsAndTrim(req.body.crm_order_number);
    payInfo.notifyURL = forexLib.removeNullsAndTrim(req.body.notify_url);
    payInfo.sign = forexLib.removeNullsAndTrim(req.body.sign);

    forexDB.confirmVerifyCode(userName, password, verifyCode, function (err) {
        if (err) {
            console.log('forex - confirm_verify_code - error - user_name:' + userName + '   password:' + password + '   verify_code:' + verifyCode + '   error:' + err);
            forexLib.log('error', 'forex - confirm_verify_code - user_name:' + userName + '   password:' + password + '   verify_code:' + verifyCode + '   error:' + err);

            if (err === 'USER_NOT_EXISTS' || err === 'WRONG_PASSWORD')
            {
                return res.send('LOGIN_FAILED');
            }

            return res.send('FAILED');
        }

        console.log('forex - confirm_verify_code - success - user_name:' + userName + '   password:' + password + '   verify_code:' + verifyCode);
        forexLib.log('success', 'forex - confirm_verify_code - user_name:' + userName + '   password:' + password + '   verify_code:' + verifyCode);

        // pay
        if (payInfo.coinType === 'BTC') {
            forexBTC.pay(userName, payInfo, function (err) {
                if (err) {
                    console.log('forex - btc.pay - error:' + err);
                    forexLib.log('error', 'forex - btc.pay - error:' + err);

                    if (err === 'NOT_ENOUGH')
                    {
                        return res.send('NOT_ENOUGH');
                    }

                    return res.send('FAILED');
                }

                console.log('forex - btc.pay - return - success');
                forexLib.log('success', 'forex - btc.pay - return - success');

                return res.send('SUCCESS');
            });
        } else if (payInfo.coinType === 'ETH') {
            forexETH.pay(userName, payInfo, function (err) {
                if (err) {
                    console.log('forex - eth.pay - error:' + err);
                    forexLib.log('error', 'forex - eth.pay - error:' + err);

                    if (err === 'NOT_ENOUGH')
                    {
                        return res.send('NOT_ENOUGH');
                    }

                    return res.send('FAILED');
                }

                console.log('forex - eth.pay - return - success');
                forexLib.log('success', 'forex - eth.pay - return - success');

                return res.send('SUCCESS');
            });
        } else if (payInfo.coinType === 'MDC') {
            forexMDC.pay(userName, payInfo, function (err) {
                if (err) {
                    console.log('forex - mdc.pay - error:' + err);
                    forexLib.log('error', 'forex - mdc.pay - error:' + err);

                    if (err === 'NOT_ENOUGH')
                    {
                        return res.send('NOT_ENOUGH');
                    }

                    return res.send('FAILED');
                }

                console.log('forex - mdc.pay - return - success');
                forexLib.log('success', 'forex - mdc.pay - return - success');

                return res.send('SUCCESS');
            });
        }
    });
};

exports.checkExternalPayment = function (req, res) {
    var payInfo = {};
    payInfo.coinType = forexLib.removeNullsAndTrim(req.body.coin_type);
    payInfo.amount = forexLib.removeNullsAndTrim(req.body.amount);
    payInfo.userId = forexLib.removeNullsAndTrim(req.body.user_id);
    payInfo.address = forexLib.removeNullsAndTrim(req.body.address);
    payInfo.crmOrderNumber = forexLib.removeNullsAndTrim(req.body.crm_order_number);

    forexDB.checkExternalPayment(payInfo, function (err, payResult) {
        if (err) {
            console.log('forex - checkExternalPayment - error:' + err);
            forexLib.log('error', 'forex - checkExternalPayment - error:' + err);

            return res.send({ status: 'error' });
        }

        if (payResult === null) {
            // console.log('forex - checkExternalPayment - waiting');
            // forexLib.log('info', 'forex - checkExternalPayment - waiting');

            return res.send({ status: 'waiting' });
        }

        console.log('forex - checkExternalPayment - success');
        forexLib.log('success', 'forex - checkExternalPayment - success');

        return res.send({ status: 'done', pay_result: payResult });
    });
};

exports.saveParams = function (req, res) {
    var params = {};
    params.coinType = forexLib.removeNullsAndTrim(req.body.coinType);
    params.amount = forexLib.removeNullsAndTrim(req.body.amount);
    params.userId = forexLib.removeNullsAndTrim(req.body.userId);
    params.userETHAddress = forexLib.removeNullsAndTrim(req.body.userETHAddress);
    params.crmOrderNumber = forexLib.removeNullsAndTrim(req.body.crmOrderNumber);
    params.notifyURL = forexLib.removeNullsAndTrim(req.body.notifyURL);
    params.sign = forexLib.removeNullsAndTrim(req.body.sign);

    forexDB.saveParams(params, function (err) {
        if (err) {
            console.log('forex - save_params - error:' + err);
            forexLib.log('error', 'forex - save_params - error:' + err);

            return res.send('FAILED');
        }

        console.log('forex - save_params - success');
        forexLib.log('success', 'forex - save_params - success');

        return res.send('SUCCESS');
    });
};

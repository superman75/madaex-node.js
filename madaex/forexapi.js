var querystring = require('querystring');
var request = require('request');
var forexCfg = require('./forexcfg');
var forexLib = require('./forexlib');

var otcURL;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_LOCAL) otcURL = forexCfg.OTC_URL_LOCAL;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_TEST) otcURL = forexCfg.OTC_URL_TEST;
if (forexCfg.PRODUCTION === forexCfg.PRODUCTION_WINDOWS) otcURL = forexCfg.OTC_URL_WINDOWS;

function callRestAPI (params, uri, callback) {
    params = querystring.stringify(params);
    var contentLength = params.length;

    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: uri,
        body: params,
        method: 'POST'
    }, function (err, res_api, body) {
        if (err) {
            console.log('forex - api_request - error - err.msg:' + err.message);
            forexLib.log('error', 'forex - api_request - err.msg:' + err.message);
            return callback(err, body);
        }

        try {
            // console.log('forex - callRestAPI - try - body:' + body);
            // forexLib.log('info', 'forex - callRestAPI - try - body:' + body);

            console.log('forex - callRestAPI - try');
            forexLib.log('info', 'forex - callRestAPI - try');

            return callback(null, JSON.parse(body));
        } catch (e) {
            console.log('forex - callRestAPI - catch - body:' + body);
            forexLib.log('error', 'forex - callRestAPI - catch - body:' + body);

            return callback(null, {status: 'failed', msg: 'JSON parse exception'});
        }
    });
};

exports.selfPost = function (params, notify_url, callback) {
    callRestAPI(params, notify_url, function (err, result) {
        return callback(err, result);
    });
};


let database        = require('./database');
let lib     =  require('./lib');
let config  = require('../config/config');
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
/*
    create a new order.
 */
exports.createOrder = function(req , res , next)
{
    let param = {};
    param.user_id       = req.user.id;
    param.market        = req.body.market;
    param.side          = req.body.side;
    param.cost          = req.body.cost;
    param.fee           = req.body.fee;
    param.volumn         = req.body.volumn;
    param.payment_method = req.body.payment_method;
    param.alipay_account = req.body.alipay_account;
    param.account_name   = req.body.account_name;
    param.account_number = req.body.account_number;
    param.routing_number = req.body.routing_number;
    param.dest_addr      = req.body.dest_addr;
    param.created_at     = new Date();
    param.status         = 0;

    if (param.side === 'sell'){
        //checking if the user have enough digital currency on his wallet.
        database.getRemainBalanceById(req.user.id, req.body.market, function(result){
            if(result.status === 'success'){
                let fee = param.volumn * result.fee;
                let evaluation;
                param.p_fee = result.fee;
                if (param.market === 'BTC' || param.market === 'ETH') {
                    fee = Math.round(fee * 10e8)/10e8;
                    evaluation = parseFloat(param.volumn) + fee;
                }
                if (param.market === 'MDC'){
                    fee = parseInt(param.fee);
                    param.volumn = parseInt(param.volumn);
                    evaluation = param.volumn + fee;
                } 
                if (result.msg < evaluation){
                    res.json({status : "failed", msg : "SMALLERTHANBALANCE."});
                } else {
                    // insert a new order to database
                    database.insertOrder(param,function(result){
                        if (result.status === 'failed'){
                            res.json(result)
                        } else {
                            database.increaseBalance(1,fee,param['market'],function(result){
                                if(result.status === 'failed'){
                                    res.json(result);
                                } else {
                                    database.decreaseBalance(param.user_id ,evaluation,param['market'],function(result){
                                        if(result.status === 'failed'){
                                            res.json(result);
                                        } else {
                                            if( param.market === 'ETH'){
                                                ////////////////////////////////////////////////////////////////////////////////////////////////
                                                var fromAccount = lib.deriveAddress(param.user_id , 'ETH');
                                                var fromAccountPassword = config.ETH_PASS;
                                                var toAccount = lib.deriveAddress(1, 'ETH');

                                                var nWeiFee = fee * 1e18;
                                                var value = web3.utils.toHex(nWeiFee);

                                                console.log('create trading : ETH : fee to admin : fee : ', nWeiFee);

                                                web3.eth.personal.unlockAccount(fromAccount, fromAccountPassword, 600).then(function(result) {
                                                    web3.eth.sendTransaction({
                                                        from: fromAccount,
                                                        to: toAccount,
                                                        value: value
                                                    })
                                                        .on('transactionHash', function(hash) {
                                                            console.log('chat - increase fee amount to company account - transactionHash');
                                                        })
                                                        .on('receipt', function(receipt)
                                                        {
                                                            console.log('chat - increase fee amount to company account - receipt');
                                                        }).catch(function(error) {});
                                                }).catch(function(error) {
                                                    console.log('error : chat - increase fee amount to company account - catch - error:' + error);
                                                });
                                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            }
                                            database.lockBalance(param.user_id ,param['volumn'],param['market'],function(result){
                                                res.json(result);
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                res.json(result);
            }
        });
    }
    else {
        database.getRemainBalanceById(req.user.id, req.body.market, function(result) {
            if(result.status === 'failed'){
                res.json(result);
            } else {
                // this is to create a new order for buyer
                param.p_fee = result.fee;
                database.insertOrder(param, function(result){
                    res.json(result);
                })
            }
        })
    }

};
/*
    get message by txid
 */
exports.getAllMessage = function (req, res) {
    console.log("ready to get all chats history by txid");
    database.getAllChatByTxid(req.body.txid, function(result){
        if(result.status === 'failed'){
            console.log("fail to get all chats history by txid");
            res.json({status : "failed", msg : result.msg})
        } else {
            console.log("success to get all chats history by txid");
            res.json({status : 'success', msg : result.msg})
        }
    })
};

exports.cancelOrder = function(req, res){
    let order_id = req.body.order_id;
    database.cancelOrder(order_id, function(result){
        if(result.status === 'failed'){
            res.json(result);
        } else {
            let remaining_balance = result.msg.remaining_volumn;
            let user_id = result.msg.user_id;
            let market = result.msg.market;
            let side = result.msg.side;
            if(side === 'buy') res.json(result);
            else {
                database.increaseBalance(user_id, remaining_balance, market, function(result){
                    res.json(result);
                })
            }
        }
    })
}
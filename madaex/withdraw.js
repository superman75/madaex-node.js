var assert  = require('assert');
var bc      = require('./bitcoin_client');
var db      = require('./database');
var remoteDb = require('./remoteDb');
var request = require('request');
var config  = require('../config/config');
var Tx      = require('ethereumjs-tx');
var util    = require('ethereumjs-util');
var lib     = require('./lib');


// use local ipc provider
// var net = require('net');
// var web3 = new Web3(new Web3.providers.IpcProvider('\\\\.\\pipe\\geth.ipc'));

// use local http provier
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

exports.withdraw = function(userid ,fromaddr,  amount , toaddr, kind , tradeId , fee ,callback)
{
    var amount1 = parseFloat(amount);

    if(kind != 3)
    {
        //Following method only insert into dep_withdraws table
        db.updateinfo(userid , amount1 , "Withdraw" /*, toaddr   , tradeId */ , kind , function(error , wid)
        {
            if (error) {return(callback({status : 'failed' , msg : 'Can not insert into dep_withdraws.'}))}
            console.log('withdraw');
            assert(wid);
            console.log("wrt : kind:", kind);
            // var amountToSend;
            //btc.
            if(kind == 1)
            {
                // var realAmountToSend = amountToSend;
                // console.log("realAmountToSend : " + realAmountToSend);
                bc.sendToAddress(toaddr, amount1, function (err, hash) {
                    if (err) {
                        console.log('btc send to address failed -> ' + err);
                        lib.log('error', 'btc send to address failed -> ' + err);
                        return callback({status: 'failed', msg: err.message});
                    }

                    db.increaseCompanyBenefit('btc', fee, function (res) {
                        if (res.status == 'failed') {
                            console.log('increase company benefit result -> ' + res);
                            lib.log('info', 'increase company benefit result -> ' + res);
                            return callback({status: 'failed', msg: 'increaseCompanyBenefit'});
                        }

                        db.setTransactionId(wid, hash, fee, function (res) {
                            if (res.status == 'failed') {
                                console.log('error : setTransactionId:' + res);
                                lib.log('error', 'setTransactionId:' + res);
                                return callback({status: 'failed', msg: 'setTransactionId'});
                            }

                            return callback({status: 'success', msg: 'Successfully withdrawed'});
                        });
                    });
                });
            }
            //eth.
            if(kind == 2)
            {
                var toSendAmount = amount1;
                toSendAmount = toSendAmount * 1e18;

                // work with loaded deposit source
                var fromAccount = lib.deriveAddress(userid , 'ETH');
                var fromAccountPassword = config.ETH_PASS;
                console.log(fromAccount + ":" + "fromAccount" );
                var value = web3.utils.toHex(toSendAmount);

                web3.eth.personal.unlockAccount(fromAccount, fromAccountPassword, 600).then(function(result) {
                    web3.eth.sendTransaction({
                        from: fromAccount,
                        to: toaddr,
                        value: value
                    })
                        .on('transactionHash', function(hash) {

                            console.log('\n Transaction sent successfully. Check the transaction hash ', hash);
                            // waitForReceipt(hash, function (receipt) {
                            //////////////////////////////////increase company benefit///////////////////////////////////
                            db.increaseCompanyBenefit('eth' , fee , function(res){
                                if (res.status === 'failed')
                                {
                                    return callback({status : 'failed' , msg : res.msg});
                                }

                                db.setTransactionId(wid , hash , fee , function(result){
                                    if (res.status === 'failed')
                                    {
                                        return callback({status : 'failed' , msg : res.msg});
                                    }

                                    return callback({status : 'success' , msg : "Successfully withdrawed"});
                                });
                            });
                        })
                        .on('receipt', function(receipt)
                        {
                            web3.eth.getBalance('' + fromAccount + '').then(function(result)
                            {
                                console.log(" Result in getBalance function ", result);
                            }).catch(function(error) {
                                console.log("Error", error);
                            });
                        }).catch(function(error) {});
                }).catch(function(error) {
                    console.log(error.message);
                    db.setTransactionId(wid , error.message , 0 , function(res){
                        return callback({status : 'failed' , msg : error.message});
                    });
                });
            }
            //game point.
        });
    }
    else
    {
        console.log('MDC withdrawal');

        var mdcFee = 0;

        db.saveMDCTransaction(userid, amount1, Math.ceil(fee), function(err, result) {
            if (err) {
                return callback({status: 'failed', msg: err});
            }
            else {
                db.transferToken(fromaddr, toaddr, amount1, false, function(err){
                    if (err) {
                        return callback({status:'failed', msg:err});
                    }
                    else {
                        db.transferToken(fromaddr, '', Math.ceil(fee), false, function(fail) {
                            if (fail) {
                                return callback({status:'failed', msg:fail});
                            }
                            else {
                                db.completeMDCTransaction(result, function(error){
                                    if (error) return callback({status : 'failed', msg : error});
                                    else return callback({status:'success', msg:''});
                                });
                            }
                        });
                    }
                });
                return callback({status: 'success', msg: result});
            }
        });

    }
};

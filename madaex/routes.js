let admin       = require('./admin');
let lib         = require('./lib');
let database    = require('./database');
let user        = require('./user');
let order       = require('./order');
let wallet      = require('./wallet');
let sendEmail   = require('./sendEmail');
let config      = require('../config/config');
let production  = process.env.NODE_ENV === 'production';
let ADMIN_PATH  = '/admin';
let mime        = require('mime-types');
let fs          = require('fs');
let url         = require('url');
var api         = require('../apis/api');
var forexpay    = require('./forexpay');
let recaptchaValidator      = require('recaptcha-validator');
let DEFAULT_REDIRECT_PATH   = '/login';

// loadStaicPage plays a role of middleware
// loadPage is function that loads specific page.
function loadStaticPage (loadPage) {
    return function (req, res) {
        let user = req.user;

        let rate = {};

        rate.btc = config.BTC_FEE;
        rate.eth = config.ETH_FEE;
        rate.mdc = config.MDC_FEE;

        console.log("This device is " + req.device.type.toUpperCase()+ ".");
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; // January is 0!

        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;

        let usdvscny = config.USDVSCNY;

        if (req.user === undefined) {
            return res.redirect(DEFAULT_REDIRECT_PATH);
        } else if (req.user.admin === '1' && req.url !== '/admin') {
            return res.redirect('/admin');
        } else if (req.user.admin === '1' && req.url === '/admin') {
            database.getUsers(function(users){
                if(users.status === 'failed') {
                    console.log(users.msg);
                    res.redirect('/error');
                }
                else {
                    database.getOrders(function(orders){
                        if(orders.status === 'failed') {
                            console.log(orders.msg);
                            res.redirect('/error');
                        }
                        else {
                            database.getTradings(function(tradings){
                                if(tradings.status === 'failed') {
                                    console.log(tradings.msg);
                                    res.redirect('/error');
                                }
                                else {
                                    database.getCommon(function(common){
                                        if(common.status === 'failed'){
                                            console.log(common.msg);
                                            res.redirect('/error');
                                        } else{
                                            database.getWidthDrawal(function(withdraw){
                                                if(withdraw.status === 'failed'){
                                                    console.log(withdraw.msg);
                                                    res.redirect('/error');
                                                } else{
                                                    return res.render('admin', {user: user, common : common.msg, users: users.msg, tradings : tradings.msg, withdraws : withdraw.msg, orders: orders.msg, rate: rate, now: today, usdvscny: usdvscny, isLogged : true});
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    });

                }
            })
        } else if (req.user.admin !== '1' && req.url === '/admin') {
            if (req.deviceType === 'PHONE'){
                res.redirect('app/login');
            } else
            return res.redirect('/login');
        } else {
            return loadPage(req, res);
        }
    };
}
function indexPage (req, res) 
{
    // console.log(req.country);
    let param = {};
    param.side = 'buy';
    param.currentpage = 0;
    param.max_count = 4;
    if (req.query.buy_page > 0){
        param.currentpage = req.query.buy_page ;
    }

    let param1 = {};
    param1.side = 'sell';
    param1.currentpage = 0;
    param1.max_count = 4;
    if (req.query.sell_page > 0){
        param1.currentpage = req.query.sell_page ;
    }
    let isLogged = false;
    // console.log(req.user);

    let user = {};
    if (req.user === undefined) {
        param.userid = -1;
        param1.userid = -1;
        isLogged = false;
    } else {
        param.userid = req.user.id;
        param1.userid = req.user.id;
        isLogged = true;
        user = req.user;
        if (user.admin === '1') {
            res.redirect('/admin');
        }
    }

    let country = req.country;
    if (!country) { country = config.DEFAULT_COUNTRY; }
    param.country = country;
    param1.country = country;

    database.getOrderBySide(param, function (error_buy, result_buy) {
        // console.log(error_buy);
        if (error_buy) return res.redirect('/error');
        else {
            database.getOrderBySide(param1, function (error_sell, result_sell) {
                if (error_sell)
                    return res.redirect('/error');
                else {
                    console.log("Hi to " + req.deviceType+ " User");
                    if (req.deviceType === 'PHONE'){
                        console.log("This is first page for phone user.")
                        return res.render('app/index', {buydata: result_buy.rows, selldata: result_sell.rows, isLogged: isLogged, user: user, country: country, sell_page : param.currentpage, buy_page :  param.currentpage});
                    } else
                    return res.render('index', {buydata: result_buy.rows, selldata: result_sell.rows, isLogged: isLogged, user: user, country: country, sell_page : param.currentpage, buy_page :  param.currentpage});
                }
            });
        }
    });
}

function aboutusPage(req, res) {
    let user = req.user;
    if (user === undefined){
        if (req.deviceType === 'PHONE'){
            res.render('app/aboutus',{isLogged : false});
        } else
        res.render('aboutus',{isLogged : false});
    } else {
        if (req.deviceType === 'PHONE'){
            res.render('app/aboutus',{user : user, isLogged : true});
        } else
        res.render('aboutus',{user : user, isLogged : true});
    }

}
function exchangeratioPage(req, res) {
    let user = req.user;
    if (user === undefined){
        if (req.deviceType === 'PHONE'){
            res.render('app/exchangeratio',{isLogged : false});
        } else
            res.render('exchangeratio',{isLogged : false});
    } else {
        if (req.deviceType === 'PHONE'){
            res.render('app/exchangeratio',{user : user, isLogged : true});
        } else
            res.render('exchangeratio',{user : user, isLogged : true});
    }


}
function faqPage(req, res) {

    let user = req.user;
    if (user === undefined){
        if (req.deviceType === 'PHONE'){
            res.render('app/faq',{isLogged : false});
        } else
            res.render('faq',{isLogged : false});
    } else {
        if (req.deviceType === 'PHONE'){
            res.render('app/faq',{user : user, isLogged : true});
        } else
            res.render('faq',{user : user, isLogged : true});
    }

}
function servicesPage(req, res) {
    let user = req.user;
    if (user === undefined){
        res.render('services',{isLogged : false});
    } else {
        res.render('services',{user : user, isLogged : true});
    }

}
function termsofservicePage(req, res) {
    let user = req.user;
    if (user === undefined){
        if (req.deviceType === 'PHONE'){
            res.render('app/termsofservice',{isLogged : false});
        } else
            res.render('termsofservice',{isLogged : false});
    } else {
        if (req.deviceType === 'PHONE'){
            res.render('app/faq',{user : user, isLogged : true});
        } else
            res.render('faq',{user : user, isLogged : true});
    }

}
function walletPage (req, res) {
    let verifycode =
        parseInt(req.query.verifycode);
    let param = {};
    let userid = req.user.id;
    param.userid = userid;
    let BTCaddr = lib.deriveAddress(userid, 'BTC');
    let ETHaddr = lib.deriveAddress(userid, 'ETH');
    let FLAG_Account = '0';
    let isLogged = true;
    database.getUserById(userid,function(err,result){
        if(err)
        {
            console.log('error' , err);
            lib.log('error' , err);
            res.json({status : 'failed', msg : "can't get user's info"});
        } else {
            if(result.email_vcode === verifycode){
                database.setEmailVerified(userid, function(error, result){
                    if(error) return res.redirect('/error');
                    else {
                        console.log("Your account is verified.")
                    }
                })
            }
            database.getMDCAddress(userid, function(error, MDCaddr) {
                if (error) return res.redirect('/error');
                    database.getWalletInfoByUserId(userid, function (result1) {
                        if (result1.status === 'success') {
                            if (req.deviceType === 'PHONE'){
                                res.render('app/wallet',{BTCAddr: BTCaddr, ETHAddr: ETHaddr, MDCAddr: MDCaddr, history: result1.msg, user: result, flag_a : FLAG_Account, isLogged : isLogged});
                            } else
                            res.render('wallet', {BTCAddr: BTCaddr, ETHAddr: ETHaddr, MDCAddr: MDCaddr, history: result1.msg, user: result, flag_a : FLAG_Account, isLogged : isLogged});
                        } else {
                            res.redirect('/error');
                        }
                    });
            });

        }

    });

}

// get order list and trading list for this user.
function dashboardPage (req, res) {
    let param = {};
    param.userid = req.user.id;
    let user = req.user;
    let isLogged = true;
    database.getPersonalOrders(param, function (error, orderdata)
    {
       if (error) return res.redirect('/error');
        database.getPersonalTradesByBuyer(param, function (err, buyerdata) 
        {
            if (err) return res.redirect('/error');
            database.getPersonalTradesBySeller(param, function (er, sellerdata) 
            {

                if (er) return res.redirect('/error');
                database.getUnreadMsgsByUserId(req.user.id, function(unreadMsgList){
                    if(unreadMsgList.status === 'failed'){
                        return res.redirect('/error');
                    }
                    console.log("unreadMsgLIst", unreadMsgList.msg);
                    return res.render('dashboard', {orderdata: orderdata, buyerdata:buyerdata,sellerdata: sellerdata, user: user, unreadMsgList : unreadMsgList.msg, isLogged : isLogged});
                });

            });
        });
    });
}


function requestTradeBuyPage (req, res) {
    let param = {};
    param.side = 'sell';
    param.currentpage = 0;
    param.userid = req.user.id;
    param.max_count = 10;
    let user = req.user;
    let isLogged = true;

    database.getOrderBySort(param, function (error_buy, result_buy) {
        if (error_buy) return res.redirect('/error');
        if (req.deviceType === 'PHONE'){
            return res.render('app/sell_buy', {data_cost: result_buy.result_cost.rows,data_credit: result_buy.result_credit.rows,  flag: 2, user: user, isLogged:isLogged});
        } else
        return res.render('sell_buy', {data_cost: result_buy.result_cost.rows,data_credit: result_buy.result_credit.rows,  flag: 2, user: user, isLogged:isLogged});
    });
}

function requestTradeSellPage (req, res) {
    let param = {};
    param.side = 'buy';
    param.currentpage = 0;
    param.userid = req.user.id;
    param.max_count = 10;
    let user = req.user;
    let isLogged = true;

    database.getOrderBySort(param, function (error_sell, result_sell) {
        if (error_sell) return res.redirect('/error');
        if (req.deviceType === 'PHONE'){
            return res.render('app/sell_buy', {data_cost: result_sell.result_cost.rows,data_credit: result_sell.result_credit.rows,  flag: 1, user: user, isLogged: isLogged});
        } else
        return res.render('sell_buy', {data_cost: result_sell.result_cost.rows,data_credit: result_sell.result_credit.rows,  flag: 1, user: user, isLogged: isLogged});
    });
}

function buyPage (req, res) {
    let order_id = req.params.orderId;
    let isLogged = false;
    if (req.user) isLogged = true;
    database.getOrderById(order_id, function (result) {
        if (result.status === 'failed') return res.redirect('/error');
        else {
           let user = req.user;
            if (req.deviceType === 'PHONE'){
                return res.render('app/newdeal', {data: result.msg, user: user, isLogged : isLogged, flag : 2});
            } else
                return res.render('newdeal', {data: result.msg, user: user, isLogged : isLogged, flag : 2});
        }
    });
}
function sellPage (req, res) {
    let order_id = req.params.orderId;
    let isLogged = false;
    if (req.user) isLogged = true;
    database.getOrderById(order_id, function ( result) {
        if (result.status === 'failed') return res.redirect('/error');
        else {
            let user = req.user;
            if (req.deviceType === 'PHONE'){
                return res.render('app/newdeal', {data: result.msg, user: user, isLogged : isLogged, flag : 1});
            } else
                return res.render('newdeal', {data: result.msg, user: user, isLogged : isLogged, flag : 1});
        }
    });
}
// On this page users can create new order
function createPage (req, res) {
    let user = req.user;
    let isLogged = true;
    let usdvscny = config.USDVSCNY;
    database.getCommon(function(result){
        if(result.status === 'failed'){
            res.redirect('/error');
        } else {
            return res.render('create', {user: user, usdvscny: usdvscny, isLogged : isLogged , common :result.msg});
        }
    });

}
// This is unused but should be here.............................................
function adminPage (req, res) {
    let user = req.user;
    let rate = {};
    rate.btc = config.BTC_FEE;
    rate.eth = config.ETH_FEE;
    rate.mdc = config.MDC_FEE;
    console.log(rate);
    return res.render('admin', {user: user, rate: rate});
}

/// ///////////////////////////////////////////////////////////////////////////////////
function tradingPage(req,res){
    let user_id = req.user.id;
    let trading_txid = req.params.trading_txid;
    database.getTradeByTxid(trading_txid, function(result){
        if (result.status === 'failed') res.json({status : 'failed', msg : result.msg});
        else {
            console.log(result.msg + '------------');
            let trading = result.msg;
            // if (trading.status !== 0 ){
            //     console.log("Rendering error on tradingPage");
            //     res.redirect('../error');
            // }
            if (user_id === trading.seller_id){
                database.getUserById(trading.buyer_id, function(error, result){
                    if(error) console.log("Database error : You can't get User by id -- tradingPage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/trading',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                        } else
                            res.render('trading',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                    }
                })
            } else if (user_id === trading.buyer_id){
                database.getUserById(trading.seller_id, function(error, result){
                    if(error)console.log("Database error : You can't get User by id -- tradingPage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/trading',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                        } else
                        res.render('trading',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                    }
                })
            } else console.log("You have no relation to this trading. -- tradingPage");
        }
    })

}

function chatPage(req,res){
    let user_id = req.user.id;
    let trading_txid = req.params.trading_txid;
    database.getTradeByTxid(trading_txid, function(result){
        if (result.status === 'failed') res.json({status : 'failed', msg : result.msg});
        else {
            console.log(result.msg + '------------');
            let trading = result.msg;
            if (user_id === trading.seller_id){
                database.getUserById(trading.buyer_id, function(error, result){
                    if(error) console.log("Database error : You can't get User by id -- chatPage");
                    else {
                        res.render('chat',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                    }
                })
            } else if (user_id === trading.buyer_id){
                database.getUserById(trading.seller_id, function(error, result){
                    if(error)console.log("Database error : You can't get User by id -- chatPage");
                    else {
                        res.render('chat',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                    }
                })
            } else console.log("You have no relation to this trading. -- chatPage");
        }
    })

}

function transferPage(req,res){
    let user_id = req.user.id;
    let trading_txid = req.params.trading_txid;
    database.getTradeByTxid(trading_txid, function(result){
        if (result.status === 'failed') res.json({status : 'failed', msg : result.msg});
        else {
            console.log(result.msg + '------------');
            let trading = result.msg;
            if (trading.status !== 1){
                console.log('Rendering error on TransferPage');
                res.redirect('../error');
            }
            if (user_id === trading.seller_id){
                database.getUserById(trading.buyer_id, function(error, result){
                    if(error) console.log("Database error : You can't get User by id -- transferPage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/transfer',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                        } else
                        res.render('transfer',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                    }
                })
            } else if (user_id === trading.buyer_id){
                database.getUserById(trading.seller_id, function(error, result){
                    if(error)console.log("Database error : You can't get User by id -- transferPage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/transfer',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                        } else
                        res.render('transfer',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                    }
                })
            } else console.log("You have no relation to this trading. -- transferPage");
        }
    })

}

function completePage(req,res){
    let user_id = req.user.id;
    let trading_txid = req.params.trading_txid;
    database.getTradeByTxid(trading_txid, function(result){
        if (result.status === 'failed') res.json({status : 'failed', msg : result.msg});
        else {
            console.log(result.msg + '------------');
            let trading = result.msg;
            if (trading.status !== 2){
                console.log('Rendering error on complete page');
                res.redirect('../error');
            }
            if (user_id === trading.seller_id){
                database.getUserById(trading.buyer_id, function(error, result){
                    if(error) console.log("Database error : You can't get User by id -- completePage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/complete',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                        } else

                            res.render('complete',{user : req.user, trading : trading, flag : 1, client : result,isLogged : true});
                    }
                })
            } else if (user_id === trading.buyer_id){
                database.getUserById(trading.seller_id, function(error, result){
                    if(error)console.log("Database error : You can't get User by id -- completePage");
                    else {
                        if (req.deviceType === 'PHONE'){
                            res.render('app/complete',{user : req.user, trading : trading, flag : 2, client : result,isLogged : true});
                        } else

                            res.render('complete',{user : req.user, trading : trading, flag :2 , client : result, isLogged: true});
                    }
                })
            } else console.log("You have no relation to this trading. -- completePage");
        }
    })

}

module.exports = function (app) {
    // get requests
    app.get('/', indexPage);
    app.get('/wallet', loadStaticPage(walletPage));
    app.get('/dashboard', loadStaticPage(dashboardPage));
    app.get('/requestTradeBuy', loadStaticPage(requestTradeBuyPage));
    app.get('/requestTradeSell', loadStaticPage(requestTradeSellPage));
    app.get('/buy/:orderId', loadStaticPage(buyPage));
    app.get('/sell/:orderId', loadStaticPage(sellPage));
    app.get('/trading/:trading_txid', loadStaticPage(tradingPage));
    app.get('/transfer/:trading_txid', loadStaticPage(transferPage));
    app.get('/complete/:trading_txid', loadStaticPage(completePage));
    app.get('/chat/:trading_txid', loadStaticPage(chatPage));
    app.get('/create', loadStaticPage(createPage));
    app.get('/admin', loadStaticPage(adminPage));

    // with out middle ware...................................................
    app.get('/login', function (req, res) {
        if (req.deviceType === 'PHONE'){
            res.render('app/login');
        } else
    	res.render('login');
    });
    app.get('/register', function (req, res) {
        if (req.deviceType === 'PHONE'){
            res.render('app/register');
        } else
    	res.render('register');
    });
    app.get('/verifycode/:userid', function (req, res) {
        let userid = req.params.userid;
        // console.log(userid);
        res.render('verifycode', {userid: userid});
    });
    app.get('/logout', user.logout);

    app.get('/error', function (req, res) {
    	res.render('error');
    });
    app.get('/forgot', function (req, res) {
        if (req.deviceType === 'PHONE'){
            res.render('app/forgot');
        } else
        res.render('forgot');
    });

    app.get('/aboutus', aboutusPage);
    app.get('/termsofservice', termsofservicePage);
    app.get('/services', servicesPage);
    app.get('/exchangeratio', exchangeratioPage);
    app.get('/faq', faqPage);

    
    app.get('/chat', function (req,res) {
       res.render('chat');
    });

    app.get('/test' , function(req, res){
        res.render('test');
    })


    app.get('/filedownload', function (req, res, next) 
    {
        console.log("download url = " , req.query.filepath);
        let file        = req.query.filepath;
        let mimetype    = mime.lookup(file);

        res.setHeader('Content-disposition', 'attachment; filename=verify.jpg');
        res.setHeader('Content-type', mimetype);

        let filestream = fs.createReadStream(file);
        filestream.pipe(res);
    });

    app.post('/getSellData', user.getSellData);
    app.post('/post_cancel', user.postCancel);

    // app.post('/getTransactionInfo' , database.getTransactionInfo);
    // app.post('/getRecentTransactionInfo' , database.getRecentTransactionInfo);

    // post requests
    //user module
    app.post('/login',              user.login);
    app.post('/register',           user.register);
    app.post('/verifycode',         user.verifycode);
    app.post('/forgot',             user.forgot);
    app.post('/recoverpassword',    user.recoverpassword);

    //order module
    app.post('/createOrder',        order.createOrder);
    app.post('/cancelOrder',        order.cancelOrder);
    //trading module
    app.post('/getAllMessage',      order.getAllMessage);

    //wallet module
    app.post('/changepassword',     wallet.changePassword);
    app.post('/uploadImage',        wallet.uploadImage);
    app.post('/test' , wallet.test);
    app.post('/uploadIdImages',     wallet.uploadIdImages);
    app.post('/updateBankInfo',     wallet.updateBankInfo);
    app.post('/updateAlipayInfo',   wallet.updateAlipayInfo);
    app.post('/withdraw',           wallet.withdraw);
    app.post('/verifyEmail',        wallet.verifyEmail);
    app.post('/updateIDInfo',       wallet.updateIDInfo);



    app.post('/chats',              user.getAllchats);
    app.post('/verifies',           user.getVerifies);
    app.post('/verificate',         user.verificate);
    app.post('/getVerifyData',      user.getVerifyData);
    app.post('/isValidPlayer',      user.isValidPlayer);
    app.post('/getAvailableAmount', user.getAvailableAmount);
    app.post('/sendToAdmin',        user.sendToAdmin);

    /*
    *Routes for admin page
    */
    app.post('/nameVerify',             admin.nameVerify);
    app.post('/setFee',                 admin.setFee);
    app.post('/setExCost',              admin.setExCost);
    app.post('/getCompanyWalletInfo',   admin.getCompanyWalletInfo);
    app.post('/getStatistic',           admin.getStatistic);
    app.post('/getTotalAmountInfo',     admin.getTotalAmountInfo);
    app.post('/companyWithdraw',        admin.companyWithdraw);

    app.post('/getAllUnreadMsg',    user.getAllUnreadMsg);
    app.post('/getNewUnreadMsg',    user.getNewUnreadMsg);
    app.post('/sendReply',          user.sendReply);
    app.post('/discardMail',        user.discardMail);

    /*
 * Routes for api
 */
    app.post('/api/getMDCAddress', api.getNewMDCAddress);
    app.post('/api/transferToken', api.transferToken);
    app.post('/api/generatingAddresses', api.generate_addresses);
    app.post('/api/getMadabitAddress', api.getMadabitAddress);

    // forex
    app.post('/forexpay', forexpay.forexpay);
    app.get('/forexpay', forexpay.forexpay_lang);
    app.post('/requestVerifyCode', forexpay.requestVerifyCode);
    app.post('/confirmVerifyCode', forexpay.confirmVerifyCode);
    app.post('/checkExternalPayment', forexpay.checkExternalPayment);
    app.post('/saveParams', forexpay.saveParams);

    // 404
    app.get('*', function (req, res) {
	        res.status(404);
	        res.render('404');
    });

    app.post('/updateWaitingData', user.updateWaitingData);
    app.post('/getMsgUrl'       , user.getMsgUrl);

    app.post('/readMsg'         , user.readMsg);
    app.post('/badgeInfo'       , user.badgeInfo);
    app.post('/urlGetAvailableAmount' , user.urlGetAvailableAmount);
    app.post('/getCurrenctFee' ,  user.getCommonInfo);
};

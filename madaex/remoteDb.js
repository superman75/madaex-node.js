//postgresql API
var assert  = require('better-assert');
var async   = require('async');
var pg      = require('pg');
var fs      = require('fs');
// var bitcoin = require('bitcoin');
var assert  = require('assert');
var lib     = require('./lib');
var config  = require('../config/config');
var passwordHash = require('password-hash');
var uuid = require('uuid');

var remoteConnector = require('../apis/api_test');

// var databaseUrl          = config.DATABASE_URL1;
// var databaseUrl1 		 = config.DATABASE_URL1;
// assert(databaseUrl);
// assert(databaseUrl1);

pg.types.setTypeParser(20, function(val)
{
    // parse int8 as an integer
    return val === null ? null : parseInt(val);
});

// function to connect with DB.
// function connect(callback)
// {
//     return pg.connect(databaseUrl, callback);
// }

// function to use sql.
// function query(query, params, callback)
// {
// 	// console.log(query);
// 	// console.log(params);
//     //thrid parameter is optional
//     if (typeof params == 'function')
//     {
//         callback = params;
//         params   = [];
//     }
//
//     connect(function(err, client, done)
//     {
//         if (err) return callback(err);
//         client.query(query, params, function(err, result)
//         {
//             done();
//             if (err)
//             {
//                 return callback(err);
//             }
//             callback(null, result);
//         });
//     });
// }

exports.getPlayerInfo = function(param , callback)
{
    var gp_username = param.gp_username;
    var gp_password = param.gp_password;
    var formData = {};
    formData.playername = gp_username;
    console.log(gp_username + ":" + gp_password);
    remoteConnector.remoteQuery(formData , 'getPlayerInfo' , function(err , res){
        if(err)
        {
            console.log(err);
            return callback({status : 'failed' , msg : "Can not get player info:" + err.message});
        }
        else
        {
            // if(res.rows.length > 0)
            // {
            // 	var pwd = res.rows[0].password;
            // 	var verified = passwordHash.verify(mdc_password, pwd);
            // 	if(verified)
            // 		return callback({status : 'success' , msg : res.rows[0]});
            // 	else
            // 		return callback({status : 'failed' , msg : 'Password incorrect'});
            // }
            // else
            // {

            // 	return callback({status : 'failed' , msg : 'There is no player with username and password'});
            // }
            // console.log(res);
            res.msg.balance_satoshis = res.msg.balance_satoshis / 1000;
            return callback(res);
        }
    });

}
exports.getRemainBalanceById = function(player_id , callback)
{
    var formData = {};
    formData.playerid = player_id;
    // console.log(mdc_username + ":" + mdc_password);
    remoteConnector.remoteQuery(formData , 'getBalanceById' , function(err , res){
        if(err)
        {
            console.log(err);
            return callback(err , null);
        }
        else
        {
            // if(res.rows.length > 0)
            // {
            // 	var pwd = res.rows[0].password;
            // 	var verified = passwordHash.verify(mdc_password, pwd);
            // 	if(verified)
            // 		return callback({status : 'success' , msg : res.rows[0]});
            // 	else
            // 		return callback({status : 'failed' , msg : 'Password incorrect'});
            // }
            // else
            // {

            // 	return callback({status : 'failed' , msg : 'There is no player with username and password'});
            // }
            // console.log(res);
            if(res.status == 'success')
            {
                res.msg = res.msg / 1000;
                return callback(null , res.msg);
            }
            else
            {
                var er = new Error(res.msg);
                return callback(err , null);
            }

        }
    });
}

exports.decreaseBalanceByGP = function(param , callback)
{
    console.log(param);
    var user_id 	= param.user_id;
    var balance_mdc 	= param.balance_mdc * 1000;
    var companyFee  = Math.ceil(param.fee * 1000);
    balance_mdc = Math.round(balance_mdc);

    var formData = {};
    formData.playerid 	= user_id;
    formData.amount 	= -1 * balance_mdc;
    formData.fee 		= companyFee;

    console.log("remoteDB" , formData);
    remoteConnector.remoteQuery(formData , 'updateBalance' , function(err , res){
        if(err)
        {
            console.log(err);
            return callback({status : 'failed' , msg : "Can not update player info:" + err.message});
        }
        else
        {
            console.log('decreased balance.');
            // if(res.rows.length > 0)
            // {
            // 	var pwd = res.rows[0].password;
            // 	var verified = passwordHash.verify(mdc_password, pwd);
            // 	if(verified)
            // 		return callback({status : 'success' , msg : res.rows[0]});
            // 	else
            // 		return callback({status : 'failed' , msg : 'Password incorrect'});
            // }
            // else
            // {

            // 	return callback({status : 'failed' , msg : 'There is no player with username and password'});
            // }
            // console.log(res);
            return callback(res);
        }
    });
}

// exports.getUserInfoByUserId = function(user_id , callback)
// {
// 	// query('select * from users where id = $1' , [user_id] , function(err , res){
// 	// 	if(err)
// 	// 	{
// 	// 		return callback({status : 'failed' , msg: 'Can not get user info by user id '});
// 	// 	}
// 	// 	else
// 	// 	{
// 	// 		var balance_mdc = res.rows[0].balance_satoshis / 1000;
// 	// 		res.rows[0].balance_satoshis = balance_mdc;
// 	// 		return callback({status : 'success' , msg : res.rows[0]});
// 	// 	}
// 	// });
// 	var formData = {};
// 	formData.playerid = player_id;
// 	// console.log(mdc_username + ":" + mdc_password);
// 	remoteConnector.remoteQuery(formData , 'getBalanceById' , function(err , res){
// 		if(err)
// 		{
// 			console.log(err);
// 			return callback({status : 'failed' , msg : "Can not get mdc balance:" + err.message});
// 		}
// 		else
// 		{
// 			// if(res.rows.length > 0)
// 			// {
// 			// 	var pwd = res.rows[0].password;
// 			// 	var verified = passwordHash.verify(mdc_password, pwd);
// 			// 	if(verified)
// 			// 		return callback({status : 'success' , msg : res.rows[0]});
// 			// 	else
// 			// 		return callback({status : 'failed' , msg : 'Password incorrect'});
// 			// }
// 			// else
// 			// {

// 			// 	return callback({status : 'failed' , msg : 'There is no player with username and password'});
// 			// }
// 			// console.log(res);
// 			res.msg = res.msg / 1000;
// 			return callback(res);
// 		}
// 	});
// }
exports.moveFromTo = function(fromAddr , toAddr , amount , fee , MINING_FEE , callback)
{
    console.log("increasecompanybenefit in remoteDb.js step : " ,
        " fromaddr = " + fromAddr +
        " toaddr   = " + toAddr   +
        " amount1  = " + amount  +
        " gpFee    = " + fee);
    lib.log("increasecompanybenefit in remoteDb.js step : " ,
        " fromaddr = " + fromAddr +
        " toaddr   = " + toAddr   +
        " amount1  = " + amount  +
        " gpFee    = " + fee);
    var increaseVal = amount + Math.ceil(fee + MINING_FEE);
    // query('update users set balance_satoshis = balance_satoshis - $1 where id = $2' , [amount , fromAddr ] , function(err , result){
    // 	if(err)
    // 	{
    // 		return callback({status : 'failed' , msg : 'Can not update users remotely-dec'});
    // 	}
    // 	else
    // 	{
    console.log(amount);
    console.log(toAddr);
    // query('update users set balance_satoshis = balance_satoshis + $1 where id = $2' , [parseInt(amount * 1000) , toAddr] , function(err1 , result1){
    // 	if(err1)
    // 	{
    // 		return callback({status : 'failed' , msg : 'Can not udpate users remotely-inc'});
    // 	}
    // 	else
    // 	{
    // 		return callback({status : 'success' , msg : ''});
    // 	}
    // });
    // 	}
    // });
    var user_id 	= toAddr;
    var balance_mdc 	= amount * 1000;
    balance_mdc 		= Math.round(balance_mdc);

    console.log('------------------------------move from to-------------------------------------------');
    console.log(balance_mdc);
    console.log(toAddr);

    var formData 		= {};
    formData.playerid 	= user_id;
    formData.amount 	= balance_mdc;
    formData.fee 		= Math.round(fee * 1000);
    console.log("increase game points step in remoteDb.js : "+
        " playerid = " , formData.playerid +
        " amount = " + formData.amount +
        " fee = " + formData.fee);
    lib.log("increase game points step in remoteDb.js : "+
        " playerid = " , formData.playerid +
        " amount = " + formData.amount +
        " fee = " + formData.fee);

    remoteConnector.remoteQuery(formData , 'updateBalance' , function(err , res)
    {
        if(err)
        {
            console.log(err);
            return callback({status : 'failed' , msg : "Can not update player info:" + err.message});
        }
        else
        {
            // if(res.rows.length > 0)
            // {
            // 	var pwd = res.rows[0].password;
            // 	var verified = passwordHash.verify(mdc_password, pwd);
            // 	if(verified)
            // 		return callback({status : 'success' , msg : res.rows[0]});
            // 	else
            // 		return callback({status : 'failed' , msg : 'Password incorrect'});
            // }
            // else
            // {

            // 	return callback({status : 'failed' , msg : 'There is no player with username and password'});
            // }
            // console.log(res);
            return callback(res);
        }
    });
}

exports.increaseCompanyBenefit = function(amount , callback)
{
    // query('update users set balance_satoshis = balance_satoshis + $1 where username = $2' , [Math.round(amount * 1000) , 'madabit'] , function(err , result){
    // 	if(err)
    // 	{
    // 		return callback({status : 'failed' , msg : 'Can not increase company benefit -' + err.message});
    // 	}
    // 	else
    // 	{
    // 		return callback({status : 'success' , msg : ''});
    // 	}
    // });
    console.log('-----------------------------------company benefit increase--------------------------------');
    console.log(amount);
    var formData = {};
    formData.amount = Math.round(amount * 1000);
    remoteConnector.remoteQuery(formData , 'increaseCompanyBalance' , function(err , res){
        if(err)
        {
            return callback({status : 'failed' , msg : 'Can not update company balance. : ' + err.message});
        }
        else
        {
            return callback(res);
        }
    });
}

exports.getCompnayWalletInfo = function(callback)
{
    // query('select balance_satoshis from users where username = $1' , ['madabit'] , function(err , res){
    // 	if(err)
    // 	{
    // 		return callback({status : 'failed' , msg : 'Can not get balance_satoshis from users-admin'});
    // 	}
    // 	else
    // 	{
    // 		return callback({status : 'success' , msg : res.rows[0].balance_satoshis / 1000});
    // 	}
    // })
    var formData = {};
    remoteConnector.remoteQuery(formData , 'getCompanyBalance' , function(err , res){
        if(err)
        {
            return callback({status : 'failed' , msg : 'Can not get company balance. : ' + err.message});
        }
        else
        {
            res.msg = res.msg / 1000;
            return callback(res);
        }
    });
}


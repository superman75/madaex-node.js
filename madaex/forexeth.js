var forexDB = require('./forexdb');
var forexLib = require('./forexlib');
// var Web3 = require('web3');

// var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
// var strETHAddress = '0xBF0Ccd9379e6436B34b7e99637D62E81D430cF5C';
// var strETHPass = '123456789';

exports.pay = function (userName, payInfo, callback) {
    forexDB.checkBalanceETH(userName, payInfo.amount, function (err) {
        if (err) {
            console.log('forex_eth.pay - error - checkBalanceETH - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            forexLib.log('error', 'forex_eth.pay - checkBalanceETH - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
            return callback(err);
        }

        // payInfo.amount = web3.utils.toHex(payInfo.amount * 1e18);
        // web3.eth.personal.unlockAccount(strETHAddress, strETHPass, 600)
        //     .then(function (result) {
        //         web3.eth.sendTransaction({
        //             from: strETHAddress,
        //             to: address,
        //             value: payInfo.amount
        //         }).on('transactionHash', function (hash) {
        //             console.log('\n Transaction sent successfully. Check the transaction hash ', hash);
        //             forexDB.payETH(userName, payInfo.amount, function (err) {
        //                 if (err) return callback(err);
        //                 return callback(null, hash);
        //             });
        //         }).catch(function (err) {
        //             console.log('forex - eth - error occurred - error:' + err);
        //             return callback(err);
        //         });
        //     }).catch(function (err) {
        //         console.log('forex - eth - error occurred while unlocking account - error:' + err);
        //         return callback(err);
        //     });

        forexDB.payETH(userName, payInfo.amount, function (err) {
            if (err) {
                console.log('forex_eth.pay - error - payETH - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                forexLib.log('error', 'forex_eth.pay - payETH - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                return callback(err);
            }
            // return callback(null, hash);

            forexDB.updateFunding(payInfo, function (err/*, payResult */) {
                if (err) {
                    console.log('forex_eth.pay - error - updateFunding - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                    forexLib.log('error', 'forex_eth.pay - updateFunding - userName:' + userName + '   amount:' + payInfo.amount + '   err:' + err);
                    return callback(err);
                }

                return callback(null/*, payResult */);
            });
        });
    });
};

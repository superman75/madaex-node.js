let fs = require('fs');

let express = require('express');
let http = require('http');
let https = require('https');
let constants = require('constants');
let compression = require('compression');
let path = require('path');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let socketIO = require('socket.io');
let ioCookieParser = require('socket.io-cookie');
let _ = require('lodash');
let debug = require('debug')('app:index');
const fileUpload = require('express-fileupload');
let app = express();
let config = require('../config/config');
let routes = require('./routes');
let database = require('./database');
let Chat = require('./chat');
let lib = require('./lib');
let ip = require('ip');
let checkip = require('check-ip');
let i18n = require('i18n-express');
let session = require('express-session');
let device = require('express-device');


// console.log("here");
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
/** TimeAgo Settings:
 * Simplify and de-verbosify timeago output.
 **/
let timeago = require('timeago');
let timeago_strings = _.extend(timeago.settings.strings, {
    seconds: '< 1 min',
    minute: '1 min',
    minutes: '%d mins',
    hour: '1 hour',
    hours: '%d hours',
    day: '1 day',
    days: '%d days',
    month: '1 month',
    months: '%d months',
    year: '1 year',
    years: '%d years'
});
timeago.settings.strings = timeago_strings;

/**
 *Printing all environment variables
 **/
/*
console.log('*********************************environment variables**************************************');
console.log(process.env);
*/

// console.log(config.BTC_FEE);
// process.env.BTC_FEE = 100;
// console.log(config.BTC_FEE);

/** Render Engine
 *
 * Put here render engine global variable trough app.locals
 * **/
app.set('views', path.join(__dirname, '../views'));

app.locals.recaptchaKey = config.RECAPTCHA_SITE_KEY;
app.locals.buildConfig = config.BUILD;
app.locals.miningFeeBits = config.MINING_FEE / 100;


let dotCaching = true;
if (!config.PRODUCTION) {
    app.locals.pretty = true;
    dotCaching = false;
}

app.engine('html', require('dot-emc').init(
    {
        app: app,
        fileExtension: 'html',
        options: {
            templateSettings: {
                cache: dotCaching
            }
        }
    }
).__express);

/** Middleware **/
app.use(bodyParser());
app.use(cookieParser());
app.use(compression());
app.use(fileUpload());
app.use(device.capture());
/** App settings **/
app.set('view engine', 'html');
app.disable('x-powered-by');
app.enable('trust proxy');

app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

app.use(i18n({
    translationsPath: path.join(__dirname, '../i18n'), // <--- use here. Specify translations files path.
    siteLangs: ['zh', 'en'],
    defaultLang: 'zh',
    textsVarName: 'translation'
}));

/** Serve Static content **/
let twoWeeksInSeconds = 1209600;
if (config.PRODUCTION) {
    app.use(express.static(path.join(__dirname, '../build'), { maxAge: twoWeeksInSeconds * 1000 }));
} else {
    app.use(express.static(path.join(__dirname, '../theme'), { maxAge: 1 })); // twoWeeksInSeconds * 1000
    app.use('/node_modules', express.static(path.join(__dirname, '../node_modules')), { maxAge: twoWeeksInSeconds * 1000 });
}

/** Login middleware
 *
 * If the user is logged append the user object to the request
 */

let strIP = ip.address();
console.log(strIP);
let bIsPublicIP = checkip(strIP).isPublicIp;
/* We manually set bIsPublicIp to true so that the server runs on HTTPS server */
bIsPublicIP = true;

app.use(function (req, res, next) {
    if (bIsPublicIP && !req.secure) {
        res.redirect('https://' + req.headers.host + req.url);
        return;
    }

    /** *******************************************Country****************************************************************/
    let sessionOptions = {
        httpOnly: true,
        // secure : config.PRODUCTION
        secure: false
    };
    req.deviceType = req.device.type.toUpperCase();

    let countrySet = req.query.country;
    let country = req.cookies.country;

    if (countrySet !== undefined) {
        let expire = new Date();
        expire.setDate(expire.getDate() + 1);
        sessionOptions.expire = expire;
        res.cookie('country', countrySet, sessionOptions);
        req.country = countrySet;
    } else {
        if (country !== undefined) {
            req.country = country;
        } else {
            let expire = new Date();
            expire.setDate(expire.getDate() + 1);
            sessionOptions.expire = expire;

            country = config.DEFAULT_COUNTRY;

            res.cookie('country', country, sessionOptions);
            req.country = country;
        }
    }

    /********************************************************************************************************************/

    let sessionId = req.cookies.id;
    // console.log('-------------------------session------------------------');
    // console.log(sessionId);
    if (!sessionId) {
        res.header('Vary', 'Accept, Accept-Encoding, Cookie');
        res.header('Cache-Control', 'public, max-age=60'); // Cache the logged-out version

        return next();

        // return res.redirect('/login');
    }

    res.header('Cache-Control', 'no-cache');
    res.header('Content-Security-Policy', "frame-ancestors 'none'");

    if (!lib.isUUIDv4(sessionId)) {
        res.clearCookie('id');
        return next();
    }

    database.getUserBySessionId(sessionId, function (err, result) {
        if (err) {
            res.clearCookie('id');
            if (err === 'NOT_VALID_SESSION') {

                return res.redirect('/');
            } else {
                console.error('[INTERNAL_ERROR] Unable to get user by session id ' + sessionId + ':', err);
                return res.redirect('/error');
            }
        }
        // user.advice = req.query.m;
        // user.error = req.query.err;
        // user.eligible = lib.isEligibleForGiveAway(user.last_giveaway);
        // user.admin = user.userclass === 'admin';
        // user.moderator = user.userclass === 'admin' ||
        //                  user.userclass === 'moderator';
        // this was defined by me : author sky
        req.user = result;
        // console.log(user);
        next();
    });
});

/** Error Middleware
 *
 * How to handle the errors:
 * If the error is a string: Send it to the client.
 * If the error is an actual: error print it to the server log.
 *
 * We do not use next() to avoid sending error logs to the client
 * so this should be the last middleware in express .
 */
function errorHandler (err, req, res, next) {
    if (err) {
        if (typeof err === 'string') {
            return res.render('error', { error: err });
        } else {
            if (err.stack) {
                console.error('[INTERNAL_ERROR] ', err.stack);
            } else console.error('[INTERNAL_ERROR', err);
            res.render('error');
        }
    } else {
        console.warning("A 'next()' call was made without arguments, if this an error or a msg to the client?");
    }
}

routes(app);
app.use(errorHandler);

/**  Server **/
let serverHttp = http.createServer(app);
serverHttp.listen(config.PORT_HTTP, function () {
    console.log('W: Listening on port ', config.PORT_HTTP, ' with HTTP');
});
let server = serverHttp;

let serverHttps;
console.log(config.USE_HTTPS);
console.log(bIsPublicIP);
if (config.USE_HTTPS && bIsPublicIP) {
    let options = {
        key: fs.readFileSync(config.HTTPS_KEY),
        cert: fs.readFileSync(config.HTTPS_CERT),
        secureProtocol: 'SSLv23_method',
        secureOptions: constants.SSL_OP_NO_SSLv3 | constants.SSL_OP_NO_SSLv2
    };

    if (config.HTTPS_CA) {
        options.ca = fs.readFileSync(config.HTTPS_CA);
    }

    serverHttps = https.createServer(options, app);
    serverHttps.listen(config.PORT_HTTPS, function () {
        console.log('W: Listening on port ', config.PORT_HTTPS, ' with HTTPS');
    });

    server = serverHttps;
}

let io = socketIO(server); // Socket io must be after the lat app.use
io.use(ioCookieParser);

/** Socket io login middleware **/
io.use(function (socket, next) {
    debug('incoming socket connection');

    let sessionId = (socket.request.headers.cookie) ? socket.request.headers.cookie.id : null;

    // If no session id or wrong the user is a guest
    if (!sessionId || !lib.isUUIDv4(sessionId)) {
        socket.user = undefined;
        return next();
    }

    database.getUserBySessionId(sessionId, function (err, user) {
        // The error is handled manually to avoid sending it into routes
        if (err) {
            if (err === 'NOT_VALID_SESSION') {
                // socket.emit('err', 'NOT_VALID_SESSION');
                // console.log('not valid session');
                next(new Error('NOT_VALID_SESSION'));
            } else {
                // console.log('unablee to get user')
                console.error('[INTERNAL_ERROR] Unable to get user in socket by session ' + sessionId + ':', err);
                next(new Error('Unable to get the session on the server, logged as a guest.'));
                // return socket.emit('err', 'INTERNAL_ERROR');
            }
            // socket.user = false;
            return next();
        }

        // Save the user info in the socket connection object
        // console.log(user);
        socket.user = user;
        // socket.user.admin = user.userclass === 'admin';
        // socket.user.moderator = user.userclass === 'admin' || user.userclass === 'moderator';
        next();
    });
});

let chatServer = new Chat(io);

/** Log uncaught exceptions and kill the application **/
process.on('uncaughtException', function (err) {
    console.error((new Date()).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    process.exit(1);
});

console.log('checking super accounts started');
lib.log('info', 'checking super accounts started');
database.createSuperAccount('madaexadmin', config.COMPANY_PASS, function (err) {
    if (err) {
        if (err === 'USERNAME_TAKEN') { console.log('checking company account : exists.'); } else { console.log('Unknown error occurred for create company account err:' + err); }
    } else {
        console.log('checking company account : created.');
    }

    database.createSuperAccount('madabit', config.MADA_PASS, function (err) {
        if (err) {
            if (err === 'USERNAME_TAKEN') { console.log('checking madabit account : exists.'); } else { console.log('Unknown error occurred for create madabit account err:' + err); }
        } else {
            console.log('checking madabit account : created.');
        }

        database.createSuperAccount('_forex', config.FOREX_PASS, function (err) {
            if (err) {
                if (err === 'USERNAME_TAKEN') { console.log('checking forex account : exists.'); } else { console.log('Unknown error occurred for create forex account err:' + err); }
            } else {
                console.log('checking forex account : created.');
            }
        });
    });
});
